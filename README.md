# TPFS CI

This repo holds common CI-related definitions and utilities that are shared across multiple repos within the
`TransparentIncDevelopment/Product` subgroup and is licensed under MIT OR Apache-2.0

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [TLDR](#tldr)
  - [Modify shared GitLab CI/CD templates](#modify-shared-gitlab-cicd-templates)
  - [Modify a build agent](#modify-a-build-agent)
- [Docker Images - Versioning Guidelines](#docker-images---versioning-guidelines)
  - [Inspecting published versions](#inspecting-published-versions)
- [GitLab CI Job Definitions and Default Pipelines](#gitlab-ci-job-definitions-and-default-pipelines)
  - [Using the default pipeline](#using-the-default-pipeline)
  - [Using a Versioned Pipeline](#using-a-versioned-pipeline)
  - [Subscribing to Auto-Updates for a Versioned Pipeline](#subscribing-to-auto-updates-for-a-versioned-pipeline)
  - [Using the default job definitions](#using-the-default-job-definitions)
  - [Overriding Default Pipeline's image.](#overriding-default-pipelines-image)
  - [Debugging CI definitions](#debugging-ci-definitions)
- [Creating a new repo](#creating-a-new-repo)
  - [Required Settings](#required-settings)
- [Honeycomb dashboards](#honeycomb-dashboards)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## TLDR

### Modify shared GitLab CI/CD templates

As explained in [GitLab CI Job Definitions and Default Pipelines](#gitlab-ci-job-definitions-and-default-pipelines), CI/CD templates maintained in this repo are reused by other repos. Since the templates are versioned, changing them requires the following changes:

- update [`VERSION.txt`](./VERSION.txt) in the feature branch before merging to `master`
- after the merge to `master`, push a new tag reflecting the new version, e.g. `v1.1.0`

The tag pipeline will take care of publishing the necessary artifacts and trigger CI/CD autoupdates across all subscribed repos.

### Modify a build agent

1. Take a `branch` of this repo to represent your work.
1. Update the docker files with the new dependency.
1. Bump the Docker image version.
    1. (see [Docker Images - Versioning Guidelines](#docker-images---versioning-guidelines))
    1. Pay special attention to the versioning guidelines for the rust build as the version follows the rust version.
1. Update `/templates/node/gitlab-ci.definitions.yaml` to point to the new node bulid image version if you changed this image.
1. Update `/templates/rust/gitlab-ci.definitions.yaml` to point to the new rust build image version if you changed this image.
1. Create an MR for your changes. 
    1. Pushing your changes will cause the creation of the new docker images.
1. Add the following yaml block to the gitlab-ci file in your client repo:

```yaml
# For node, use the rust definitions if appropriate
include:
    - project: 'transparentincdevelopment/product/devops/tpfs-ci'
      ref: <tfsp-ci repo branch>
      file: '/templates/node/gitlab-ci.definitions.yaml'
```

10. Test your changes.
1. If your test works, get MR approval and merge your changes to master.
1. In order to automatically receive updates to the gitlab ci definitions, update the gitlab-ci file in your client repo as follows:

```yaml
# For node, use the rust definitions if appropriate
include:
    - project: 'transparentincdevelopment/product/devops/tpfs-ci'
      ref: master
      file: '/templates/node/gitlab-ci.definitions.yaml'
```

## Docker Images - Versioning Guidelines

Under `./docker` you will find common docker images used for CI. Names and tags (version) for all images are kept in `./docker/.env`, and are
what's used when building and publishing the corresponding image.

**Any** updates to Dockerfiles should be packaged with an updated tag in `./docker/.env`, else the new docker image will not get built.

If the same image and tag exists in our [`xand-keeper` container registry](https://hub.docker.com/u/xandkeeper), a new
image is not built nor published. Repos consuming these images can pin themselves to a specific version if need be (more info below).

### Inspecting published versions

To see which image versions have already been published, use Google Container Registry (GCR)'s [CLI](https://cloud.google.com/sdk/gcloud/reference/container) to inspect container images.

[Relevant StackOverflow topic](https://stackoverflow.com/c/transparent/questions/47)

## GitLab CI Job Definitions and Default Pipelines

Under `./templates`, you will find default pipelines and CI job definitions used across many smaller
repos. Per-repo type (rust vs npm), the job definitions are in a file named `gitlab-ci.definitions.yaml`, and the
default pipeline is in a file named `gitlab-ci.yaml`.

Via GitLab's [remote inclusion](https://docs.gitlab.com/ee/ci/yaml/#includeremote), a repo can
pull in the default pipeline, or pull in the definitions file to explicitly set jobs to run or not.

See [npm templates README.md](/templates/npm/README.md) for details on the default npm CI pipeline.

### Using the default pipeline

A rust repo pulls in the default pipeline by pasting the following into its `.gitlab-ci.yaml` file:
```yaml
include:
  - project: 'transparentincdevelopment/product/devops/tpfs-ci'
    ref: master
    file: '/templates/rust/gitlab-ci.yaml'
```

### Using a Versioned Pipeline

`tpfs-ci` is shifting toward a versioning and auto-updating scheme.  In order to use this scheme, import the pipeline into a file called `.ci/tpfs.yaml` per the initial implementation described in the [CI AutoUpdate README](/ci_autoupdate/README.md).  e.g.:

```yaml
# .ci/tpfs.yaml
include:
  - project: 'transparentincdevelopment/product/devops/tpfs-ci'
    ref: v0.0.1
    file: '/templates/rust/gitlab-ci.yaml'
```

```yaml
# .gitlab-ci.yaml
include:
  - local: '/.ci/tpfs.yaml'
...
```

### Subscribing to Auto-Updates for a Versioned Pipeline

Using a version reference instead of a reference to `master` will stabilize a project's pipeline when the pipeline changes.  It is the responsibility of the project maintainers to update the reference as necessary.  

To opt-in a project for automatically-generated CI update MRs, follow the instructions in the [CI Autoupdate Subscriptions README](ci_autoupdate/subscriptions/README.md)

### Using the default job definitions

The job definitions file defines the core jobs a repo would want to run, by defining them as
[_hidden jobs_](https://docs.gitlab.com/ee/ci/yaml/#hidden-keys-jobs) (prefixed with a `.`).
If a rust repo pulls in the definitions file, it would need to explicitly "enable" desired jobs by
creating a non-hidden job that extends the hidden job by name. For example:

```yaml
include:
  - project: 'transparentincdevelopment/product/devops/tpfs-ci'
    ref: master
    file: '/templates/rust/gitlab-ci.definitions.yaml'

# Explicitly enabling the `.build-template` job and naming it `build`
build:
    extends: .build-template
```

You can customize a repo's CI pipeline by pulling in the definitions file and selecting which jobs to
incorporate.

The default job templates use scripts embedded in the Docker images to trace CI events. If you need
to override any of the script stages, due to [limitations in GitLab's YAML templating](https://docs.gitlab.com/ee/ci/yaml/#include),
you will likely need to manually copy any relevant build steps from [here](https://gitlab.com/TransparentIncDevelopment/product/devops/tpfs-ci/-/blob/master/templates/rust/gitlab-ci.definitions.yaml)
for Rust or [here](https://gitlab.com/TransparentIncDevelopment/product/devops/tpfs-ci/-/blob/master/templates/node/gitlab-ci.definitions.yaml)
for Node.js. YAML anchors, beginning with a `*`, will NOT work when referencing anchors defined in
the template; these will need to be copied manually. Alternatively, if the configuration inheritance
can be expressed using GitLab CI's [`extends`](https://docs.gitlab.com/ee/ci/yaml/#extends)
parameter and the template is generally useful you may add it to the appropriate
`gitlab-ci.definitions.yaml` and integrate it like the example above.

### Overriding Default Pipeline's image.

If pulling in the default pipeline, a repo's CI will also be run in newer CI images as they are updated. This will
be common as we incorporate newer Rust versions.

You can pin a repo to a specific CI image by overriding the variable specifying the CI image. Continuing with
the rust example, this is `TPFS_RUST_CI_IMG`. The CI image for the whole pipeline can be set with:

```yaml
# Overriding the variable specifying the image to run jobs in
variables:
  TPFS_RUST_CI_IMG: "xandkeeper/my-custom-image:some-tag"

include:
  - project: 'transparentincdevelopment/product/devops/tpfs-ci'
    ref: master
    file: '/templates/rust/gitlab-ci.yaml'
```

### Debugging CI definitions

Following the CI definitions manually as it pulls in remote files can be painful. Fortunately GitLab's CI Lint
resolves these definitions for debugging purposes. For a particular repo, append `/-/ci/lint` to the repo url
or find the *CI Lint* button in the top right of the *CI / CD -> Pipelines* page.

Copy and paste the contents of the `.gitlab-ci.yaml` file to see the resolved pipeline definition, or any errors.

> Note that if you are referencing a _local_ file using GitLab's
> [`include: local` feature](https://docs.gitlab.com/ee/ci/yaml/#includelocal), the CI Lint validator
> will only check the repo's default branch. For debugging these cases, you can update to
> include the file by full `project`, `ref`, and `file`, if need be.

## Creating a new repo

We use project templates with our required default settings to initialize new repos. When creating a new repo under the `Product`
subgroup, select **Create from template**, and view available TPFS templates in the **Group** tab.

Available templates:

- Rust Lib: https://gitlab.com/TransparentIncDevelopment/product/libs/gitlab_project_templates/rust_project_template
- npm: Coming soon! (TODO: https://www.pivotaltracker.com/story/show/171455252)

While mostly all settings and content carry over when creating from templates, there is 1 known issue
in creating from templates. The "No. approvals required" for Merge Requests gets reset to 0.

Please manually set this for the repo in **Settings** -> **General** -> **Merge Request Approvals** ->  *No. approvals required = 2*

As we scope down permissions in GitLab, you may not see the Settings tab per repo. Please message the #ci-issues Slack channel to request to update this setting.

_TODO: Address these manual needs with automation: https://www.pivotaltracker.com/story/show/171455443_

### Required Settings

This is the list of required settings for `Product` repos:

- **Settings** -> **General** -> **Merge Requests**
    - Choose _Merge commit with semi-linear history_
    - Check _Pipelines Must Succeed for MR_
    - Check _Enable 'Delete source branch' option by default_
- **Settings** -> **General** -> **Merge Request Approvals**
    - No. approvals required == 2
    - Uncheck "Can override approvers and approvals required per merge request"
    - Uncheck "Remove all approvals in a merge request when new commits are pushed.."
    - Check "Prevent approval of merge requests by merge request author"
- **Settings** -> **Repository** -> **Protected Branches**
    - Protect stable branch (master)
    - _Allowed to merge_ for protected branch set to *Developers + Maintainers*
    - _Allowed to push_ permissions for protected branch set to *No one*
- **Settings** -> **CI / CD** -> **General Pipelines**
    - _Custom CI configuration path_ == `.gitlab-ci.yaml`
- **Settings** -> **Integrations** -> *Project services*
    - Slack notifications (Copy [settings from an existing repo](https://gitlab.com/TransparentIncDevelopment/product/apps/thermite/-/settings/integrations))


## Honeycomb dashboards

Honeycomb dashboards and their queries are commonly represented in JSON. Our intended configuration is declared [here](./config/honeycomb/boards.json). The JSON configuration was created by pretty-printing the response from Honeycomb's [listing all boards](https://docs.honeycomb.io/api/boards-api/#list-all-boards) API.

Fetch the JSON list of existing boards with:
```bash
$ curl -q -H "X-Honeycomb-Team: $HONEYCOMB_API_KEY" https://api.honeycomb.io/1/boards | jq . > ./config/honeycomb/boards.json
```

If you add a JSON board definition to `./config/honeycomb/boards.json`, sync it to our instance with:
```bash
./scripts/sync-honeycomb-config.sh $HONEYCOMB_API_KEY
```

Having a version-controlled copy gives us freedom to experiment and break things in Honeycomb because the scheduled daily job reverts detected changes. The job may also be executed manually on [this page](https://gitlab.com/TransparentIncDevelopment/product/devops/tpfs-ci/pipeline_schedules).

In case the sync job finds any modified boards it also prints a JSON diff to the logs and saves an artifact of the board's JSON. For example, after a query was manually deleted from a dashboard in Honeycomb the scheduled job produced this [artifact](https://gitlab.com/TransparentIncDevelopment/product/devops/tpfs-ci/-/jobs/475415053/artifacts/browse/config/honeycomb/board-diffs/). To make changes persist they can be incorporated into the version-controlled configuration.
