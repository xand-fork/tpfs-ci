#!/usr/bin/env bash

set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -eo pipefail # fail fast with exit code, even on pipelined commands

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by CI automation and assumes
    \$CI_PIPELINE_ID and \$CI_JOB_ID are defined according to GitLab CI's
    conventions.

    Purpose: gathers and reports useful metrics on long-running script
    commands executed during CI jobs.

    Arguments:
        NAME (Required) = Short, distinctive description for the command.
        CMD_AND_ARGS (Required) = Long-running command and its arguments to execute.

    Examples:
        $0 run-build cargo build
        $0 run-tests cargo test
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

NAME=${1:?"$( error 'NAME must be set' )"}
shift
CMD_AND_ARGS=${@:?"$( error 'CMD_AND_ARGS must be set' )"}


################################################################################
# Verify ambient dependencies

if [ -z $CI_PIPELINE_ID ]; then
    echo "ERROR: \$CI_PIPELINE_ID is not set."
    exit 1
fi

if [ -z $CI_JOB_ID ]; then
    echo "ERROR: \$CI_JOB_ID is not set."
    exit 1
fi

if [ ! -x "$(command -v buildevents)" ]; then
    echo "ERROR: 'buildevents' is not installed to PATH. See https://github.com/honeycombio/buildevents#installation."
    exit 1
fi

if [ ! -x "$(command -v ts)" ]; then
    echo "ERROR: 'ts' is not installed to PATH. It is commonly packaged with 'moreutils'."
    exit 1
fi


################################################################################
# Execute using `buildevents` and `ts`

buildevents cmd "$CI_PIPELINE_ID" "$CI_JOB_ID" "$NAME" -- $CMD_AND_ARGS 2>&1 | ts -s
