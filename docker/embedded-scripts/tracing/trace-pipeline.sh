#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by CI automation.

    Arguments
        GITLAB_PROJECT_ID (Required) = Corresponds to \$CI_PROJECT_ID during CI
        GITLAB_PIPELINE_ID (Required) = Corresponds to \$GITLAB_PIPELINE_ID during CI
	RW_GITLAB_API_TOKEN (Required) = GitLab API access token with 'api' scope
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

GITLAB_PROJECT_ID=${1:?"$( error 'GITLAB_PROJECT_ID must be set' )"}
GITLAB_PIPELINE_ID=${2:?"$( error 'GITLAB_PIPELINE_ID must be set' )"}
RW_GITLAB_API_TOKEN=${3:?"$( error 'RW_GITLAB_API_TOKEN must be set' )"}


################################################################################
# Request context from GitLab API

GITLAB_API_V4_URL="https://gitlab.com/api/v4"

PIPELINE_JSON=$(curl -H "PRIVATE-TOKEN: ${RW_GITLAB_API_TOKEN}" "${GITLAB_API_V4_URL}/projects/${GITLAB_PROJECT_ID}/pipelines/${GITLAB_PIPELINE_ID}")
echo "pipeline json: $(echo $PIPELINE_JSON | jq .)"

PIPELINE_JOB_FAILS_JSON=$(curl -H "PRIVATE-TOKEN: ${RW_GITLAB_API_TOKEN}" -g "${GITLAB_API_V4_URL}/projects/${GITLAB_PROJECT_ID}/pipelines/${GITLAB_PIPELINE_ID}/jobs?scope[]=failed&scope[]=canceled&scope[]=skipped")
echo "pipeline job fails: $(echo $PIPELINE_JOB_FAILS_JSON | jq .)"


################################################################################
# Extract information needed by `buildevents`

PIPELINE_CREATED_AT=$(date --date="$(echo $PIPELINE_JSON | jq -r .created_at)" +%s)
JOB_FAIL_COUNT=$(echo $PIPELINE_JOB_FAILS_JSON | jq -r '. | length')
PASS_FAIL=$(if (( $JOB_FAIL_COUNT == 0 )); then echo "success"; else echo "failure"; fi)

echo "created at: $PIPELINE_CREATED_AT"
echo "fail count: $JOB_FAIL_COUNT"
echo "pass/fail: $PASS_FAIL"


################################################################################
# Use `buildevents` to report top-level pipeline trace span to Honeycomb

TRACE_URL=$(buildevents build $GITLAB_PIPELINE_ID $PIPELINE_CREATED_AT $PASS_FAIL)
echo "Honeycomb trace: $TRACE_URL"
