#!/usr/bin/env bash

set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -eo pipefail # fail fast with exit code, even on pipelined commands
set -x # print commands

# Installs `ts` utility, used for timing commands
apt-get update -yqq
apt-get install -yqq moreutils

# buildevents can collect trace data during CI and forward to honeycomb.io
SCRIPT_LOCATION="$( dirname "${BASH_SOURCE[0]}" )"
INSTALL_DEST="$SCRIPT_LOCATION/buildevents"
curl -L -o "$INSTALL_DEST" https://github.com/honeycombio/buildevents/releases/download/v0.4.10/buildevents-linux-amd64
chmod 755 "$INSTALL_DEST"

# Copy buildevents CLI to `/usr/local/bin`
cp $INSTALL_DEST /usr/local/bin

echo "Installed buildevents CLI at path: /usr/local/bin"
