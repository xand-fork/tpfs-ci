#!/usr/bin/env bash

set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -eo pipefail # fail fast with exit code, even on pipelined commands

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by CI automation and assumes
    \$CI_PIPELINE_ID and \$CI_JOB_ID are defined according to GitLab CI's
    conventions.

    Purpose: gathers and reports useful metrics on CI jobs.

    Arguments:
        START_TIME (Required) = POSIX timestamp denoting when the CI job began.
        JOB_NAME (Required) = Short, distinctive description for the command.

    Examples:
        $0 1583940907 build
        $0 1583940959 test
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

START_TIME=${1:?"$( error 'START_TIME must be set' )"}
JOB_NAME=${2:?"$( error 'JOB_NAME must be set' )"}


################################################################################
# Verify ambient dependencies

if [ -z $CI_PIPELINE_ID ]; then
    echo "ERROR: \$CI_PIPELINE_ID is not set."
    exit 1
fi

if [ -z $CI_JOB_ID ]; then
    echo "ERROR: \$CI_JOB_ID is not set."
    exit 1
fi

if [ ! -x "$(command -v buildevents)" ]; then
    echo "ERROR: 'buildevents' is not installed to PATH. See https://github.com/honeycombio/buildevents#installation."
    exit 1
fi


################################################################################
# Execute using `buildevents`

buildevents step $CI_PIPELINE_ID $CI_JOB_ID $START_TIME $JOB_NAME
