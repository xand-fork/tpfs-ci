#!/usr/bin/env bash

set -o errexit 	 # abort on nonzero exitstatus
set -o nounset 	 # abort on unbound variable
set -eo pipefail # abort on errors

function helptext {
HELPTEXT="
    This script is intended to be run by a scheduled GitLab CI job. It
    keeps versioned-controlled dashboard configurations in sync with
    Honeycomb.

    Arguments
	HONEYCOMB_API_KEY (Required) = API key for honeycomb.io
"
echo "$HELPTEXT"
}

function error {
    echo "$1"
    exit 1
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    error "$(helptext)

Missing arguments"
fi

HONEYCOMB_API_KEY=${1:?"$( error 'HONEYCOMB_API_KEY must be set' )"}

ERRORS_ENCOUNTERED=false
TPFS_CI_DIR="$(dirname ${BASH_SOURCE[0]})/.."
HONEYCOMB_CONFIG_DIR="${TPFS_CI_DIR}/config/honeycomb"
BOARDS_JSON_VERSIONED="${HONEYCOMB_CONFIG_DIR}/boards.json"
BOARDS_JSON_CURRENT="${HONEYCOMB_CONFIG_DIR}/boards.current.json"
BOARDS_JSON_DIFFS_DIR="${HONEYCOMB_CONFIG_DIR}/board-diffs"

if ! curl -sf -H "X-Honeycomb-Team: $HONEYCOMB_API_KEY" \
     -o "$BOARDS_JSON_CURRENT" \
     "https://api.honeycomb.io/1/boards"
then
    echo "ERROR: failed to download Honeycomb boards"
    exit 1
fi

# extract board IDs
readarray -t versioned_ids < <(cat "$BOARDS_JSON_VERSIONED" | jq -c -r ".[] | [.id] | .[]")
readarray -t current_ids < <(cat "$BOARDS_JSON_CURRENT" | jq -c -r ".[] | [.id] | .[]")

echo "versioned board IDs: ${versioned_ids[@]}"
echo "current board IDs: ${current_ids[@]}"

mkdir -p "$BOARDS_JSON_DIFFS_DIR"

for versioned_id in "${versioned_ids[@]}"; do
    echo "board ${versioned_id}: ensuring Honeycomb state corresponds to version-controlled dashboard configurations"

    json_versioned="${HONEYCOMB_CONFIG_DIR}/versioned-${versioned_id}.json"
    json_current="${HONEYCOMB_CONFIG_DIR}/${versioned_id}.json"

    # extract versioned dashboard
    cat "$BOARDS_JSON_VERSIONED" | jq ".[] | select(.id==\"${versioned_id}\")" > "$json_versioned"

    # download current dashboard
    if curl -sf -H "X-Honeycomb-Team: $HONEYCOMB_API_KEY" \
	    -o "$json_current" \
	    "https://api.honeycomb.io/1/boards/${versioned_id}"
    then
	echo "board ${versioned_id}: checking for differences between current board on Honeycomb and what's versioned ..."
	if ! json-diff "$json_versioned" "$json_current"; then
    	    echo "board ${versioned_id}: reverting to version controlled board configuration"

    	    if ! curl -sf -X PUT \
    		 -H "X-Honeycomb-Team: $HONEYCOMB_API_KEY" \
    		 -d "@${json_versioned}" \
    		 "https://api.honeycomb.io/1/boards/${versioned_id}"
	    then
		ERRORS_ENCOUNTERED=true
		echo "ERROR: failed to update board ${versioned_id}"
	    fi

	    echo "board ${versioned_id}: copying manually modified configuration to ${BOARDS_JSON_DIFFS_DIR}"
	    cp "$json_current" "${BOARDS_JSON_DIFFS_DIR}/"
	else
    	    echo "board ${versioned_id}: no differences found"
	fi
    else
	echo "board ${versioned_id}: not found on Honeycomb; applying versioned configuration"

    	if ! curl -sf -X POST \
    	     -H "X-Honeycomb-Team: $HONEYCOMB_API_KEY" \
	     -o "$json_current" \
    	     -d "$(cat "${json_versioned}" | jq "del(.id)")" \
    	     "https://api.honeycomb.io/1/boards"
	then
	    ERRORS_ENCOUNTERED=true
	    echo "ERROR: failed to replace board ${versioned_id}"
	else
	    # Although the new board ID doesn't match what's
	    # configured, everything else is the same. This is not an
	    # error condition. 
	    new_id=$(cat "${json_current}" | jq -r ".id")
	    new_config="${versioned_id}-${new_id}.json"

	    echo "board ${versioned_id}: new copy has id ${new_id}; copying config to ${BOARDS_JSON_DIFFS_DIR}"
	    cp "$json_current" "${BOARDS_JSON_DIFFS_DIR}/${new_config}"
	fi
    fi
done


for current_id in "${current_ids[@]}"; do
    if ! [[ "$versioned_ids[@]" =~ "$current_id" ]]; then
	echo "board ${current_id}: is not versioned; backing up its configuration and deleting from Honeycomb"

	echo "board ${current_id}: copying unversioned configuration to ${BOARDS_JSON_DIFFS_DIR}"
	if ! curl -sf -H "X-Honeycomb-Team: $HONEYCOMB_API_KEY" \
	     -o "${BOARDS_JSON_DIFFS_DIR}/${current_id}.json" \
	     "https://api.honeycomb.io/1/boards/${current_id}"
	then
	    ERRORS_ENCOUNTERED=true
	    echo "ERROR: failed to backup unversioned board ${current_id} configuration"
	fi

	if ! curl -sf -X DELETE \
    	     -H "X-Honeycomb-Team: $HONEYCOMB_API_KEY" \
	     "https://api.honeycomb.io/1/boards/${current_id}"
	then
	    ERRORS_ENCOUNTERED=true
	    echo "ERROR: failed to delete unversioned board ${current_id} on Honeycomb"
	else
	    echo "board ${current_id}: deleted from Honeycomb"
	fi
    fi
done

if [[ $(find "${BOARDS_JSON_DIFFS_DIR}" -type f | wc -l) -gt 0 ]]; then
    echo "Detected differences between current dashboard and the version-controlled configuration at ${BOARDS_JSON_VERSIONED}. Reverted to the version-controlled configuration. Unversioned configurations were copied to ${BOARDS_JSON_DIFFS_DIR} for review and potential versioning."
fi

if $ERRORS_ENCOUNTERED; then
    error "See ERROR messages"
else
    echo "Success!"
fi
