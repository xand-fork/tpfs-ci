# TPFS-CI Auto-Update Subscription

To subscribe your project for automatic CI updates, add an entry to CI AutoUpdate's [`subscriptions/subscriptions.toml`](subscriptions.toml).

Each entry must include the gitlab ID of the project, any [Gitlab Labels](https://gitlab.com/groups/TransparentIncDevelopment/-/labels) to mark the MR with, as well as any options for the auto-update tool (this may be dependent on the type of template used)

> **Note**: Why Subscribe?
>
> Subscribing your project to this auto-update tool will allow you to stabilize your project's pipeline by referencing it by a version rather than the `master` branch. When TPFS-CI is tagged with a version it will trigger an auto-update process for your project to use the new pipeline.  Without subscribing, you will have to manually maintain your pipeline's versioned imports.

## Add a Badge to a Subscribed Project

To add a badge to your subscribed project, go to the project's `Settings > General > Badges` section, and add these options:

- Name: CI Autoupdate
- URL: https://gitlab.com/TransparentIncDevelopment/product/devops/tpfs-ci/-/tree/master/ci_autoupdate/subscriptions
- Image URL: https://img.shields.io/badge/CI%20Autoupdate-Subscribed-blue

The badge will look like this: 

![](https://img.shields.io/badge/CI%20Autoupdate-Subscribed-blue)

This will help other maintainers know that the CI references are automatically managed when they work in a subscribed repository.

## Rust Projects

For `rust` template projects, add an entry to the `rust` table in `subscriptions.toml`:

```toml
[[rust]]
id=<gitlab project ID>
labels=[<gitlab Label Name>,... (default none)]
```

## NPM Projects

For `node` template projects, add an entry to the `npm` table in `subscriptions.toml`:

```toml
[[npm]]
id=<gitlab project ID>
labels=[<gitlab Label Name>,... (default none)]
```



