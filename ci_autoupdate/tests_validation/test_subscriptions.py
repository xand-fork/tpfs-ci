from ci_autoupdate.constants import NPM_PROJECT_TOML_KEY, RUST_PROJECT_TOML_KEY, SUBSCRIPTION_LIST_FILE
from ci_autoupdate.gitlab_actions import try_create_gitlab_connection
from ci_autoupdate.subscribed_project.deserialization import (
    deserialize_subscribed_projects,
    read_subscribed_projects_file,
)


def test_subscriptions_file_exists():
    assert SUBSCRIPTION_LIST_FILE.exists()


def test_subscription_file_all_projects_can_be_read():
    toml_dict = read_subscribed_projects_file()
    subscriptions = deserialize_subscribed_projects(toml_dict)
    npm_projects = toml_dict.get(NPM_PROJECT_TOML_KEY, [])
    rust_projects = toml_dict.get(RUST_PROJECT_TOML_KEY, [])
    assert subscriptions.__len__() == npm_projects.__len__() + rust_projects.__len__()


def test_all_subscribed_projects_exist():
    toml_dict = read_subscribed_projects_file()
    subscriptions = deserialize_subscribed_projects(toml_dict)
    connection = try_create_gitlab_connection()
    assert connection is not None
    for project in subscriptions:
        gitlab_project = connection.projects.get(project.id)
        assert gitlab_project.get_id() == project.id
