import os

from ci_autoupdate.constants import GITLAB_ACCESS_TOKEN_ENV_VAR_NAME
from ci_autoupdate.gitlab_actions import try_create_gitlab_connection


def test_gitlab_ci_autoupdate_token_is_set():
    assert os.getenv(GITLAB_ACCESS_TOKEN_ENV_VAR_NAME) is not None


def test_gitlab_connection_can_be_created():
    connection = try_create_gitlab_connection()
    assert connection is not None
    assert connection.user is not None
