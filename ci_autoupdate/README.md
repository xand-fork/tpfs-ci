# CI AutoUpdate Tool

This project is a simple tool that updates references to `tpfs-ci` imports to the latest tagged version.  It uses the list of subscribed projects and options in the [`tpfs-ci/ci_autoupdate/subscriptions`](./subscriptions) project as candidates for automatic updates.

For each project, this tool will:
- Update the TPFS-CI import file to include a reference to the new tagged version
- Automatically commit and push the updates to a new branch
- Automatically create and tag an MR for the update
- Log each step's outcome

# Contributing to CI AutoUpdate

The CI AutoUpdate has effort tracked as part of a large feature to improve CI/CD.  See [TODOs.md](TODOs.md) for a list of prioritized improvements to both the CI AutoUpdate tool and the TPFS-CI project as a whole.  All effort is initially placed in ADO, so if you choose to take on some work you have a good idea of what needs to be done and how to track it. 

# Using the Tool for Your Project

To enable the tool to work properly, you'll need to import `tpfs-ci` templates in your project via a file manually created called: `.ci/tpfs.yaml`.  Inside that file, import using this syntax:

```yaml
#.ci/tpfs.yaml
include:
  - project: 'transparentincdevelopment/product/devops/tpfs-ci'
    ref: v[VERSION]
    file: '/templates/[TEMPLATE_TYPE]/gitlab-ci.yaml'
```

where `[VERSION]` is the version of the templates you wish to initially use and `[TEMPLATE_TYPE]` is the name of the template subfolder from `tpfs-ci` for your project (e.g. `rust` or `node`).

The autoupdate tool will update this file with new version references automatically as long as this convention is followed.

For example, to import v0.0.1 tpfs-ci templates for your node project:

```yaml
#.ci/tpfs.yaml
include:
  - project: 'transparentincdevelopment/product/devops/tpfs-ci'
    ref: v0.0.1
    file: '/templates/node/gitlab-ci.yaml'
```

```yaml
#.gitlab-ci.yml
include:
  - local: '/.ci/tpfs.yaml'
    ...
```

# Dependencies

- Python 3.9+
- [Poetry 1.1.8+](https://python-poetry.org/docs/master/#installation)
- Git
- A gitlab access token environment variable set to `TPFS_GITLAB_CI_ACCESS_TOKEN`
- A gitlab username for the access token environment variable set to `TPFS_GITLAB_CI_ACCESS_USERNAME`

> If you have multiple python versions installed, you can specify a particular version to use for this project with
> ```bash
> poetry env use <path/to/python> # e.g. /usr/bin/python3.9
> ```

# Commands

## Poetry Commands

- `poetry install` - Set up environment and install dependencies
- `poetry run autoupdate <args>` - Run the project (Can also be run via `poetry run python ci_autoupdate/run.py <args>`) - e.g. `poetry run autoupdate v0.0.2`
- `poetry run test` - Run all unit tests
- `poetry run pytest-watch` - Watch all unit tests
- `poetry run test-integ` Run all integration tests
- `poetry run test-validation` Run all validation tests for subscribed projects
- `poetry run get-subscribers` Print project names and URLs information of subscribers for quick reference
- `poetry run lint` - Run flake8 linter for all source
- `poetry run update-snapshots` - Update generated snapshots for any snapshot tests
- `poetry run format` - Automatically fix lint errors in source
- `poetry run typecheck` - Run pyright type checker

## Makefile Commands

The Makefile has commands for convenient building and publishing of the Docker image for CI Autoupdate.

- `make build TARGET=(STABLE or custom string)` - Will build the ci-autoupdate docker image.  If `TARGET` is set to STABLE, it will build both the latest version tag and `latest` tag images.  If `TARGET` is set to anything else, that will be the tag used.
- `make publish TARGET=(STABLE or custom string)` - publishes the docker image to xandkeeper/ci-autoupdate.  If `TARGET` is set to `STABLE` both `latest` and `version` tags will be published.

## Docker Commands

To run the docker version of CI AutoUpdate, the command is:

```bash
docker run --network host -e TPFS_GITLAB_CI_ACCESS_TOKEN=$TPFS_GITLAB_CI_ACCESS_TOKEN -e TPFS_GITLAB_CI_ACCESS_USERNAME=$TPFS_GITLAB_CI_ACCESS_USERNAME -it xandkeeper/ci-autoupdate:<TAG>  
```

Make sure the command *is not logged* as it passes environment variables to the image.

The entrypoint of the image is poetry, so refer to [Poetry Commands] above for how to run.

Example which runs the tool for tpfs-ci version v0.0.1:

```bash
docker run --network host -e TPFS_GITLAB_CI_ACCESS_TOKEN=$TPFS_GITLAB_CI_ACCESS_TOKEN -e TPFS_GITLAB_CI_ACCESS_USERNAME=$TPFS_GITLAB_CI_ACCESS_USERNAME -it xandkeeper/ci-autoupdate:latest run autoupdate v0.0.1
```

Example which runs the tool's unit tests:

```bash
docker run --network host -e TPFS_GITLAB_CI_ACCESS_TOKEN=$TPFS_GITLAB_CI_ACCESS_TOKEN -e TPFS_GITLAB_CI_ACCESS_USERNAME=$TPFS_GITLAB_CI_ACCESS_USERNAME -it xandkeeper/ci-autoupdate:latest run test
```

## Integration tests Gitlab auth
Integration tests rely on a personal Gitlab token. In this case, a special Gitlab user was created, and a personal token
created from that user is used for these integration tests. 

If it expires, follow this to refresh it:
1. Retrieve login info for Gitlab user. Look up secret in 1Password: "Gitlab CI User"
2. Login to gitlab with that user's email and password
3. Click user profile icon in upper right
4. Click Preferences
5. Click Access Tokens
6. Create a new token with "api" access and to never expire
7. Record token code and name
8. Logout of Gitlab
9. Login back into Gitlab as yourself
10. Navigate to Transparent group CI-CD settings: https://gitlab.com/groups/TransparentIncDevelopment/-/settings/ci_cd
11. Expand Variables
12. Update variable "TPFS_GITLAB_CI_ACCESS_TOKEN" with the code
13. Update variable "TPFS_GITLAB_CI_ACCESS_TOKEN_NAME" with the name
14. Done.
