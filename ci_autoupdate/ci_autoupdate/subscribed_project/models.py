from enum import Enum
from pathlib import Path
from typing import List


class ProjectType(Enum):
    NONE = 1
    RUST = 2
    NPM = 3
    PYTHON = 4


class SubscribedProject:
    def __init__(self, id: int, project_type: ProjectType, labels: List[str] = []):
        self.id = id
        self.labels = labels
        self.project_type = project_type


class ClonedProject:
    def __init__(
        self,
        project: SubscribedProject,
        local_dir: Path,
        default_branch: str,
    ):
        self.project = project
        self.local_dir = local_dir
        self.default_branch = default_branch
