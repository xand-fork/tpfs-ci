import argparse
from pathlib import Path

from ci_autoupdate.constants import (
    CLI_ARG_POSITIONAL_NEW_VERSION,
    CLI_ARG_VERBOSE_LONG,
    CLI_ARG_VERBOSE_SHORT,
    DEFAULT_BRANCH_PREFIX,
    PROJECT_CLONE_DIR,
    SUBSCRIPTION_LIST_FILE,
)


class AutoUpdateConfig:
    def __init__(
        self,
        new_version: str,
        verbose: bool,
        branch_name_prefix: str = DEFAULT_BRANCH_PREFIX,
        subscription_list_location: Path = SUBSCRIPTION_LIST_FILE,
        local_clone_directory: Path = PROJECT_CLONE_DIR,
    ):
        self.verbose = verbose
        self.new_version = new_version
        self.new_branch_name_prefix = (
            "" if branch_name_prefix == "" else f"{branch_name_prefix.strip()}-".replace(" ", "-")
        )
        self.new_branch_name = f"{self.new_branch_name_prefix}autoupdate"
        self.subscription_list_location = subscription_list_location
        self.local_clone_directory = local_clone_directory


def get_config() -> AutoUpdateConfig:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        CLI_ARG_POSITIONAL_NEW_VERSION, type=str.lower, help="New version reference for TPFS-CI in the form 'vx.y.z'"
    )
    parser.add_argument(
        f"-{CLI_ARG_VERBOSE_SHORT}",
        f"--{CLI_ARG_VERBOSE_LONG}",
        dest=CLI_ARG_VERBOSE_LONG,
        help="The verbose option will enable logging for the DEBUG level.",
        action="store_true",
    )
    args = parser.parse_args()
    return AutoUpdateConfig(getattr(args, CLI_ARG_POSITIONAL_NEW_VERSION), getattr(args, CLI_ARG_VERBOSE_LONG))
