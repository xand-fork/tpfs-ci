import os
from datetime import datetime
from pathlib import Path
from typing import List, Union
from urllib.parse import ParseResult, urlparse

from git.repo import Repo
from gitlab import Gitlab  # type: ignore

from ci_autoupdate.constants import (
    GITLAB_ACCESS_TOKEN_ENV_VAR_NAME,
    GITLAB_ACCESS_TOKEN_USERNAMENAME_ENV_VAR_NAME,
    PROJECT_CLONE_DIR,
)
from ci_autoupdate.git.actor import GitRepoImpl
from ci_autoupdate.git.coordinator import Coordinator
from ci_autoupdate.subscribed_project.models import ClonedProject, SubscribedProject
from ci_autoupdate.tool_logging import AutoUpdateEvent, AutoUpdateEventResult, AutoUpdateLogger


def log_begin_project_clone(id: int, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_debug(f"Cloning project {str(id)}...", AutoUpdateEvent.PROJECT_CLONE)


def log_project_clone_success(project, id: int, clone_dir: str, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_info(
            f"Project {str(project.name)} (ID: {str(id)} cloned to {clone_dir}",
            AutoUpdateEvent.PROJECT_CLONE,
            AutoUpdateEventResult.SUCCESS,
        )


def log_project_clone_failure(
    id: int, error_message: str = "(No Error Message Provided!)", logger: AutoUpdateLogger = None
):
    if logger is not None:
        logger.log_error(
            f"Project {str(id)} failed to be cloned! The project will be skipped! Error message: {error_message}",
            AutoUpdateEvent.PROJECT_CLONE,
            AutoUpdateEventResult.FAILURE,
        )


def create_clone_url(
    gitlab_project_http_url_to_repo: str,
    auth_username_env_key: str = GITLAB_ACCESS_TOKEN_USERNAMENAME_ENV_VAR_NAME,
    auth_access_token_env_key: str = GITLAB_ACCESS_TOKEN_ENV_VAR_NAME,
) -> str:
    # git clone https://username:token@gitlab.com/user/repo.git
    url_parse: ParseResult = urlparse(gitlab_project_http_url_to_repo)
    username = str(os.getenv(auth_username_env_key))
    token = str(os.getenv(auth_access_token_env_key))
    clone_url = f"{url_parse.scheme}://{username}:{token}@{url_parse.netloc}{url_parse.path}"
    return clone_url


class CloneResult:
    def __init__(self, clone_local_path: Path, clone_default_branch: str) -> None:
        self.local_path = clone_local_path
        self.default_branch = clone_default_branch


def try_clone_project(
    gitlab_connection: Gitlab, id: int, clone_dir: Path = PROJECT_CLONE_DIR, logger: AutoUpdateLogger = None
) -> Union[CloneResult, None]:
    log_begin_project_clone(id, logger)
    try:
        gitlab_project = gitlab_connection.projects.get(id)
        clone_url = create_clone_url(gitlab_project.http_url_to_repo)
        unique_label = datetime.now().strftime("_%d-%m-%Y_%H-%M-%S")
        clone_path: Path = clone_dir.absolute() / (gitlab_project.path + unique_label)
        Repo.clone_from(clone_url, clone_path)
        log_project_clone_success(gitlab_project, id, str(clone_path), logger)
        return CloneResult(clone_path, gitlab_project.default_branch)
    except Exception as ex:
        log_project_clone_failure(id, str(ex), logger)
        return None


def clone_gitlab_projects(
    projects: List[SubscribedProject],
    gitlab_connection: Gitlab,
    cloned_projects_dir: Path = PROJECT_CLONE_DIR,
    logger: AutoUpdateLogger = None,
) -> List[ClonedProject]:
    cloned_projects: List[ClonedProject] = []
    for project in projects:
        clone_result = try_clone_project(gitlab_connection, project.id, cloned_projects_dir, logger)
        if clone_result is not None:
            cloned_projects.append(ClonedProject(project, clone_result.local_path, clone_result.default_branch))
    return cloned_projects


class PreparationResults:
    def __init__(
        self,
        successfully_prepared_projects: List[ClonedProject],
        failed_projects: List[ClonedProject],
    ):
        self.successfully_prepared_projects = successfully_prepared_projects
        self.failed_npm_projects = failed_projects


def prepare_updated_projects_merge_requests(
    projects: List[ClonedProject],
    new_version: str,
    new_branch_name: str,
    logger: AutoUpdateLogger = None,
) -> PreparationResults:
    successful = []
    failure = []
    for project in projects:
        if prepare_updated_project_merge_request(
            project.local_dir, project.project.labels, new_version, new_branch_name, logger
        ):
            successful.append(project)
        else:
            failure.append(project)
    return PreparationResults(successful, failure)


def prepare_updated_project_merge_request(
    cloned_project_dir: Path,
    cloned_project_labels: List[str],
    new_version: str,
    new_branch_name: str,
    logger: AutoUpdateLogger = None,
) -> bool:
    git_actor = GitRepoImpl(cloned_project_dir)
    git_coordinator = Coordinator(cloned_project_labels, git_actor, new_version, new_branch_name, logger=logger)
    result = git_coordinator.prep_merge_request()
    return result.branch_result and result.commit_result and result.push_result
