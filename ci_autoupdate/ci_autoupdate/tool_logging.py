import logging
import os
import sys
from datetime import datetime
from enum import Enum
from pathlib import Path

from ci_autoupdate.constants import LOG_OUTPUT_DIR


class AutoUpdateEvent(Enum):
    NONE = 1
    SUBSCRIPTION_PARSE = 2
    GITLAB_AUTHENTICATION = 3
    PROJECT_CLONE = 4
    BRANCH_CREATION = 5
    CI_IMPORT_UPDATE = 6
    OPTIONS_APPLIED = 7
    CHANGESET_COMMIT = 8
    BRANCH_PUSH = 9
    MR_CREATION = 10
    PIPELINE_FINISH = 11
    TAG_VALIDATION = 12


class AutoUpdateEventResult(Enum):
    NONE = 1
    SUCCESS = 2
    FAILURE = 3


class AutoUpdateLoggerOutputFile:
    def __init__(
        self,
        output_file_dir: Path = LOG_OUTPUT_DIR,
        output_file_name: str = datetime.now().strftime("%d-%m-%Y_%H-%M-%S.log"),
    ):
        self.abs = Path(output_file_dir, output_file_name).absolute()
        if not output_file_dir.absolute().exists():
            os.mkdir(output_file_dir.absolute())
        if not self.abs.exists():
            open(self.abs, "x")


class AutoUpdateLogger:
    def __init__(self, output_file: AutoUpdateLoggerOutputFile = None, verbose: bool = False):
        self.formatter = logging.Formatter(
            "%(asctime)s:%(levelname)s:[%(filename)s:%(funcName)s:%(lineno)s]:%(message)s"
        )
        logger = logging.getLogger(__name__)
        if output_file is not None:
            self.output_file = output_file
            file_log_handler = logging.FileHandler(self.output_file.abs)
            file_log_handler.setFormatter(self.formatter)
            logger.addHandler(file_log_handler)
        self.formatted_logger = logger
        self.verbose = verbose

    def __get_message(
        self,
        message: str,
        event: AutoUpdateEvent = AutoUpdateEvent.NONE,
        result: AutoUpdateEventResult = AutoUpdateEventResult.NONE,
    ) -> str:
        return f"{message} (Event: {event.name}, Result: {result.name})"

    def __set_console_handler(self):
        console_log_handler = logging.StreamHandler(sys.stdout)
        console_log_handler.setFormatter(self.formatter)
        self.formatted_logger.addHandler(console_log_handler)

    def __reset_console_handler(self, handler):
        self.formatted_logger.removeHandler(handler)

    def log_debug(
        self,
        message: str,
        event: AutoUpdateEvent = AutoUpdateEvent.NONE,
        result: AutoUpdateEventResult = AutoUpdateEventResult.NONE,
    ):
        stream_handler = self.__set_console_handler()
        self.formatted_logger.setLevel(logging.DEBUG)
        self.formatted_logger.debug(self.__get_message(message, event, result))
        self.__reset_console_handler(stream_handler)

    def log_info(
        self,
        message: str,
        event: AutoUpdateEvent = AutoUpdateEvent.NONE,
        result: AutoUpdateEventResult = AutoUpdateEventResult.NONE,
    ):
        stream_handler = self.__set_console_handler()
        self.formatted_logger.setLevel(logging.INFO)
        self.formatted_logger.info(self.__get_message(message, event, result))
        self.__reset_console_handler(stream_handler)

    def log_warning(
        self,
        message: str,
        event: AutoUpdateEvent = AutoUpdateEvent.NONE,
        result: AutoUpdateEventResult = AutoUpdateEventResult.NONE,
    ):
        stream_handler = self.__set_console_handler()
        self.formatted_logger.setLevel(logging.WARNING)
        self.formatted_logger.warning(self.__get_message(message, event, result))
        self.__reset_console_handler(stream_handler)

    def log_error(
        self,
        message: str,
        event: AutoUpdateEvent = AutoUpdateEvent.NONE,
        result: AutoUpdateEventResult = AutoUpdateEventResult.NONE,
    ):
        stream_handler = self.__set_console_handler()
        self.formatted_logger.setLevel(logging.ERROR)
        self.formatted_logger.error(self.__get_message(message, event, result))
        self.__reset_console_handler(stream_handler)

    def log_critical(
        self,
        message: str,
        event: AutoUpdateEvent = AutoUpdateEvent.NONE,
        result: AutoUpdateEventResult = AutoUpdateEventResult.NONE,
    ):
        stream_handler = self.__set_console_handler()
        self.formatted_logger.setLevel(logging.CRITICAL)
        self.formatted_logger.critical(self.__get_message(message, event, result))
        self.__reset_console_handler(stream_handler)
