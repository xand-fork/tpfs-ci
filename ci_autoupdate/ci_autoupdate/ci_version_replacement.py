from pathlib import Path
from typing import List

import yaml

from ci_autoupdate.constants import EXPECTED_TPFS_CI_IMPORT_FILE, TPFS_CI_YAML_PARENT_KEY, TPFS_CI_YAML_VERSION_KEY
from ci_autoupdate.subscribed_project.models import ClonedProject
from ci_autoupdate.tool_logging import AutoUpdateEvent, AutoUpdateEventResult, AutoUpdateLogger


def log_replacement_start(filepath: Path, new_version: str, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_debug(
            f"Replacing CI import with new version {new_version} at file: {str(filepath.absolute())}",
            AutoUpdateEvent.CI_IMPORT_UPDATE,
        )


def log_replacement_success(filepath: Path, new_version: str, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_info(
            f"Updated to new version {new_version} at file: {str(filepath.absolute())}",
            AutoUpdateEvent.CI_IMPORT_UPDATE,
            AutoUpdateEventResult.SUCCESS,
        )


def log_replacement_failure(filepath: Path, new_version: str, error_message: str, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_error(
            f"Could not update to new version {new_version} at file location {str(filepath.absolute())}. Error message: {error_message}.  Further processing of this project will be skipped!",  # noqa
            AutoUpdateEvent.CI_IMPORT_UPDATE,
            AutoUpdateEventResult.FAILURE,
        )


class VersionReplacementResults:
    def __init__(
        self,
        succeeded_projects: List[ClonedProject],
        failed_projects: List[ClonedProject],
    ):
        self.succeeded_projects = succeeded_projects
        self.failed_projects = failed_projects


def replace_ci_version_in_projects(
    cloned_projects: List[ClonedProject],
    new_version: str,
    ci_file: Path = EXPECTED_TPFS_CI_IMPORT_FILE,
    logger: AutoUpdateLogger = None,
) -> VersionReplacementResults:
    succeeded_projects: List[ClonedProject] = []
    failed_projects: List[ClonedProject] = []
    for project in cloned_projects:
        if try_replace_ci_version(new_version, project.local_dir, ci_file, logger):
            succeeded_projects.append(project)
        else:
            failed_projects.append(project)
    return VersionReplacementResults(succeeded_projects, failed_projects)


def try_replace_ci_version(
    new_version: str,
    cloned_project_dir: Path,
    ci_file: Path = EXPECTED_TPFS_CI_IMPORT_FILE,
    logger: AutoUpdateLogger = None,
) -> bool:
    """Replaces the version imported with a new version in the specified TPFS-CI import file.

    Returns:
    Success or failure boolean of the operation
    """
    full_file_Path = cloned_project_dir.absolute() / ci_file
    log_replacement_start(full_file_Path, new_version, logger)
    try:
        with open(full_file_Path, "r") as tpfs_ci_yaml_file:
            loaded_yaml = yaml.safe_load(tpfs_ci_yaml_file)
            updated_yaml = replace_ref_in_yaml(loaded_yaml, new_version)
        with open(full_file_Path, "w") as tpfs_ci_yaml_file:
            yaml.dump(updated_yaml, tpfs_ci_yaml_file, default_flow_style=False)
        log_replacement_success(full_file_Path, new_version, logger)
        return True
    except Exception as ex:
        log_replacement_failure(full_file_Path, new_version, str(ex), logger)
        return False


def replace_ref_in_yaml(loaded_yaml, new_version: str):
    for index, _ in enumerate(loaded_yaml[TPFS_CI_YAML_PARENT_KEY]):
        loaded_yaml[TPFS_CI_YAML_PARENT_KEY][index][TPFS_CI_YAML_VERSION_KEY] = new_version
    return loaded_yaml
