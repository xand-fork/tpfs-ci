import os
from typing import List, Union

from gitlab import Gitlab, GitlabAuthenticationError  # type: ignore
from gitlab.v4.objects.merge_requests import ProjectMergeRequest
from gitlab.v4.objects.projects import Project  # type: ignore

from ci_autoupdate.constants import GITLAB_ACCESS_TOKEN_ENV_VAR_NAME, GITLAB_SERVER_URL, TPFS_CI_PROJECT_ID
from ci_autoupdate.subscribed_project.models import ClonedProject
from ci_autoupdate.tool_logging import AutoUpdateEvent, AutoUpdateEventResult, AutoUpdateLogger


def try_create_gitlab_connection(
    server_url: str = GITLAB_SERVER_URL,
    access_token_env_key: str = GITLAB_ACCESS_TOKEN_ENV_VAR_NAME,
    logger: AutoUpdateLogger = None,
) -> Union[Gitlab, None]:
    try:
        gitlab_connection = Gitlab(server_url, private_token=os.getenv(access_token_env_key))
        gitlab_connection.auth()
        if logger is not None:
            logger.log_info(
                f"Connected and authenticated with user: {gitlab_connection.user.username}",
                AutoUpdateEvent.GITLAB_AUTHENTICATION,
                AutoUpdateEventResult.SUCCESS,
            )
        return gitlab_connection
    except GitlabAuthenticationError as ex:
        if logger is not None:
            logger.log_critical(
                f"Could not authenticate the Gitlab connection: {ex.error_message}",
                AutoUpdateEvent.GITLAB_AUTHENTICATION,
                AutoUpdateEventResult.FAILURE,
            )
        raise ex
    except Exception as ex:
        if logger is not None:
            logger.log_critical(
                f"Could not create a Gitlab connection: {str(ex)}",
                AutoUpdateEvent.GITLAB_AUTHENTICATION,
                AutoUpdateEventResult.FAILURE,
            )


def log_begin_new_tag_verification(project_id: int, new_version: str, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_debug(
            f"Validating that project {str(project_id)} has supplied tag {new_version}...",
            AutoUpdateEvent.TAG_VALIDATION,
        )


def log_new_tag_verification_failure(
    project_id: int,
    new_version: str,
    error_message: str = "(No Error Message Provided!)",
    logger: AutoUpdateLogger = None,
):
    if logger is not None:
        logger.log_critical(
            f"Failed to validate that the given new CI version {new_version} exists in project {str(project_id)}!  The autoupdater will abort!  Error message: {error_message}",  # noqa
            AutoUpdateEvent.TAG_VALIDATION,
            AutoUpdateEventResult.FAILURE,
        )


def log_new_tag_verification_success(new_version: str, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_info(
            f"Validated that tag {new_version} exists.",
            AutoUpdateEvent.TAG_VALIDATION,
            AutoUpdateEventResult.SUCCESS,
        )


def log_mr_creation_success(id: int, mr: ProjectMergeRequest, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_info(
            f"Successfully create a Gitlab MR for project ID {str(id)}",
            AutoUpdateEvent.MR_CREATION,
            AutoUpdateEventResult.SUCCESS,
        )
        logger.log_debug(
            f"MR id: {str(mr.get_id())}",
            AutoUpdateEvent.MR_CREATION,
        )


def log_mr_creation_failure(id: int, error_msg: str, logger: AutoUpdateLogger = None):
    if logger is not None:
        logger.log_error(
            f"Failed to create a Gitlab MR for project ID {str(id)}!  Error message: {error_msg}",
            AutoUpdateEvent.MR_CREATION,
            AutoUpdateEventResult.FAILURE,
        )


def try_check_new_ci_tag_exists(
    gitlab_connection: Gitlab, new_version: str, project_id: int = TPFS_CI_PROJECT_ID, logger: AutoUpdateLogger = None
) -> bool:
    log_begin_new_tag_verification(project_id, new_version, logger)
    try:
        tpfs_ci_project = gitlab_connection.projects.get(project_id)
        tpfs_ci_tags = tpfs_ci_project.tags.list(all=True)
        for tag in tpfs_ci_tags:
            if tag.name == new_version:
                log_new_tag_verification_success(new_version, logger)
                return True
        log_new_tag_verification_failure(
            project_id, new_version, "Tag does not exist in the project's tag list.", logger
        )
        return False
    except Exception as ex:
        log_new_tag_verification_failure(project_id, new_version, ex.__str__(), logger)
        return False


def create_merge_request(
    gitlab_connection: Gitlab,
    project_id: int,
    source_branch: str,
    target_branch: str,
    title: str,
    description: str,
    labels: List[str],
) -> ProjectMergeRequest:
    """Given merge request details, returns merge request instance"""
    project: Project = gitlab_connection.projects.get(project_id)
    mr: ProjectMergeRequest = project.mergerequests.create(
        {
            "source_branch": source_branch,
            "target_branch": target_branch,
            "title": title,
            "labels": labels,
            "description": description,
            "remove_source_branch": True,
        }
    )
    mr.approvals.set_approvers(approvals_required=0)
    return mr


def create_merge_requests(
    projects: List[ClonedProject],
    gitlab_connection: Gitlab,
    new_version: str,
    new_branch_name: str,
    logger: AutoUpdateLogger = None,
):
    for project in projects:
        try:
            mr = create_merge_request(
                gitlab_connection,
                project.project.id,
                new_branch_name,
                project.default_branch,
                f"CI Autoupdate to {new_version}",
                "This MR was created automatically from your subscription to the CI Autoupdate tool.",
                project.project.labels,
            )
            log_mr_creation_success(project.project.id, mr, logger)
        except Exception as ex:
            log_mr_creation_failure(project.project.id, ex.__str__(), logger)
