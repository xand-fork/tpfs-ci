import abc
from pathlib import Path

from git.repo.base import Repo


class GitRepoInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def path(self) -> Path:
        raise NotImplementedError

    @abc.abstractmethod
    def checkout_new_branch(self, new_branch_name: str):
        raise NotImplementedError

    @abc.abstractmethod
    def commit_changes(self, commit_msg: str):
        raise NotImplementedError

    @abc.abstractmethod
    def push(self):
        raise NotImplementedError


class GitRepoImpl(GitRepoInterface):
    def __init__(self, local_path: Path):
        self.local_path = local_path

    def path(self) -> Path:
        return self.local_path

    def checkout_new_branch(self, new_branch_name: str):
        repo = Repo(self.path())
        repo.git.checkout("-b", new_branch_name)

    def commit_changes(self, commit_msg: str):
        repo = Repo(self.path())
        repo.git.add(".")
        repo.git.commit("-m", commit_msg)

    def push(self):
        repo = Repo(self.path())
        current_branch = repo.git.branch("--show-current")
        repo.git.push("origin", current_branch)
