from ci_autoupdate.ci_version_replacement import replace_ci_version_in_projects
from ci_autoupdate.cli_args import AutoUpdateConfig, get_config
from ci_autoupdate.git_actions import clone_gitlab_projects, prepare_updated_projects_merge_requests
from ci_autoupdate.gitlab_actions import (
    create_merge_requests,
    try_check_new_ci_tag_exists,
    try_create_gitlab_connection,
)
from ci_autoupdate.subscribed_project.deserialization import (
    deserialize_subscribed_projects,
    read_subscribed_projects_file,
)
from ci_autoupdate.tool_logging import AutoUpdateLogger, AutoUpdateLoggerOutputFile


def main():
    autoupdate_config = get_config()
    run_logger = AutoUpdateLogger(AutoUpdateLoggerOutputFile(), autoupdate_config.verbose)
    run_tool(autoupdate_config, run_logger)


def run_tool(autoupdate_config: AutoUpdateConfig, run_logger: AutoUpdateLogger = None):
    gitlab_connection = try_create_gitlab_connection(logger=run_logger)
    if gitlab_connection is None:
        exit(1)
    if not try_check_new_ci_tag_exists(gitlab_connection, autoupdate_config.new_version, logger=run_logger):
        exit(1)
    subscription_toml = read_subscribed_projects_file(autoupdate_config.subscription_list_location, run_logger)
    subscribed_projects = deserialize_subscribed_projects(subscription_toml, run_logger)
    cloned_projects = clone_gitlab_projects(
        subscribed_projects, gitlab_connection, autoupdate_config.local_clone_directory, run_logger
    )
    replacement_results = replace_ci_version_in_projects(
        cloned_projects, autoupdate_config.new_version, logger=run_logger
    )
    preparation_results = prepare_updated_projects_merge_requests(
        replacement_results.succeeded_projects,
        autoupdate_config.new_version,
        autoupdate_config.new_branch_name,
        run_logger,
    )
    create_merge_requests(
        preparation_results.successfully_prepared_projects,
        gitlab_connection,
        autoupdate_config.new_version,
        autoupdate_config.new_branch_name,
        run_logger,
    )


if __name__ == "__main__":
    main()
