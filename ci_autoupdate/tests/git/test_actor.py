import os
from pathlib import Path
from tempfile import TemporaryDirectory

from git.repo.base import Repo

from ci_autoupdate.git.actor import GitRepoImpl


def initialize_git_repo() -> TemporaryDirectory:
    dir = TemporaryDirectory()
    print(dir.name)
    repo = Repo.init(dir.name)
    empty_filename = os.path.join(dir.name, "temp.txt")

    # This creates an empty file in the new dir...
    open(empty_filename, "wb").close()

    repo.index.add([empty_filename])
    repo.index.commit("Initial commit")
    current_branch = repo.active_branch
    print("Initial branch", current_branch)
    return dir


def test_checkout_new_branch__updates_current_branch():
    # Given
    dir = initialize_git_repo()
    actor = GitRepoImpl(dir.name)
    branch_name = "my-new-branch"

    # When
    actor.checkout_new_branch(branch_name)

    # Then
    current_branch = Repo(dir.name).active_branch.name
    assert current_branch == branch_name


def test_commit_changes__commits_new_file_with_msg():
    # Given
    dir = initialize_git_repo()
    dir_name = dir.name
    actor = GitRepoImpl(Path(dir_name))

    new_file = os.path.join(dir_name, "new_file.txt")
    with open(new_file, "wb") as f:
        f.write(b"some file content!")
    repo = Repo(dir_name)
    assert repo.untracked_files == ["new_file.txt"]
    commit_msg = "New commit msg"

    # When
    actor.commit_changes(commit_msg)

    # Then
    assert repo.untracked_files == []
    assert repo.head.commit.message == "New commit msg\n"


def test_commit_changes__commits_modified_file_with_msg():
    # Given
    dir = initialize_git_repo()
    dir_name = dir.name
    actor = GitRepoImpl(Path(dir_name))

    # Overwrite existing file.
    existing_file = os.path.join(dir_name, "temp.txt")
    assert os.path.isfile(existing_file)

    with open(existing_file, "wb") as f:
        f.write(b"some different content :O")
    repo = Repo(dir_name)
    assert repo.is_dirty()
    commit_msg = "New commit msg"

    # When
    actor.commit_changes(commit_msg)

    # Then
    assert not repo.is_dirty()
    assert repo.head.commit.message == "New commit msg\n"
