import toml

from ci_autoupdate.constants import ID_TOML_KEY, LABELS_TOML_KEY, NPM_PROJECT_TOML_KEY, RUST_PROJECT_TOML_KEY
from ci_autoupdate.subscribed_project.deserialization import deserialize_subscribed_project_from_toml_dict
from ci_autoupdate.subscribed_project.models import ProjectType


def test_deserialize_subscribed_project_parses_fully_given_rust_project():
    # Given
    toml_str = """[[rust]]
id=333
labels=["Deliverance Team", "Confidential Team"]"""
    toml_dict = toml.loads(toml_str)
    rust_project_toml_dict = toml_dict[RUST_PROJECT_TOML_KEY][0]
    # When
    result = deserialize_subscribed_project_from_toml_dict(rust_project_toml_dict, ProjectType.RUST)
    # Then
    assert result.project is not None
    assert result.unrecognized_keys.__len__() == 0
    assert result.missing_keys.__len__() == 0
    assert result.project.id == 333
    assert result.project.labels == ["Deliverance Team", "Confidential Team"]


def test_project_info_from_toml_dict_parses_fully_given_npm_project():
    # Given
    toml_str = """[[npm]]
id=2
labels=["Deliverance Team"]
"""
    toml_dict = toml.loads(toml_str)
    npm_project_toml_dict = toml_dict[NPM_PROJECT_TOML_KEY][0]
    # When
    result = deserialize_subscribed_project_from_toml_dict(npm_project_toml_dict, ProjectType.NPM)
    # Then
    assert result.project is not None
    assert result.unrecognized_keys.__len__() == 0
    assert result.missing_keys.__len__() == 0
    assert result.project.id == 2
    assert result.project.labels == ["Deliverance Team"]


def test_project_info_from_toml_dict_detects_missing_id():
    # Given
    toml_str = """[[rust]]
labels=["Deliverance Team"]

[[npm]]
id=3
labels=["Deliverance Team"]
"""
    toml_dict = toml.loads(toml_str)
    rust_project_toml_dict = toml_dict[RUST_PROJECT_TOML_KEY][0]
    # When
    result = deserialize_subscribed_project_from_toml_dict(rust_project_toml_dict, ProjectType.RUST)
    # Then
    assert result.unrecognized_keys.__len__() == 0
    assert result.missing_keys.__len__() == 1
    assert ID_TOML_KEY in result.missing_keys
    assert result.project is None


def test_project_info_from_toml_dict_detects_missing_labels():
    # Given
    toml_str = """[[rust]]
wrong-label=false

[[npm]]
id=4
labels=["Deliverance Team"]
"""
    toml_dict = toml.loads(toml_str)
    rust_project_toml_dict = toml_dict[RUST_PROJECT_TOML_KEY][0]
    # When
    result = deserialize_subscribed_project_from_toml_dict(rust_project_toml_dict, ProjectType.RUST)
    # Then
    assert result.unrecognized_keys.__len__() == 1
    assert result.missing_keys.__len__() == 2
    assert ID_TOML_KEY in result.missing_keys
    assert LABELS_TOML_KEY in result.missing_keys
    assert result.project is None


def test_project_info_from_toml_dict_parses_fully_given_project_without_options():
    # Given
    toml_str = """[[rust]]
id=444
labels=["Deliverance Team", "Confidential Team"]"""
    toml_dict = toml.loads(toml_str)
    rust_project_toml_dict = toml_dict[RUST_PROJECT_TOML_KEY][0]
    # When
    result = deserialize_subscribed_project_from_toml_dict(rust_project_toml_dict, ProjectType.RUST)
    # Then
    assert result.project is not None
    assert result.unrecognized_keys.__len__() == 0
    assert result.missing_keys.__len__() == 0
    assert result.project.id == 444
    assert result.project.labels == ["Deliverance Team", "Confidential Team"]
