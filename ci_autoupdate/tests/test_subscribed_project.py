import toml

from ci_autoupdate.subscribed_project.deserialization import deserialize_subscribed_projects


def test_deserialize_subscribed_projects_reads_toml_for_default_and_supplied_options_of_rust_and_npm_projects():
    # Given
    toml_str = """[[rust]]
id=29136820
labels=["Deliverance Team"]

[[rust]]
id=29
labels=["Deliverance Team", "Bar Team"]

[[npm]]
id=29136913
labels=["Deliverance Team"]

[[npm]]
id=291
labels=["Deliverance Team", "Foo Team"]
"""
    toml_dict = toml.loads(toml_str)
    # When
    subscriptions = deserialize_subscribed_projects(toml_dict)
    # Then
    rust_project_0 = subscriptions[0]
    rust_project_1 = subscriptions[1]
    npm_project_0 = subscriptions[2]
    npm_project_1 = subscriptions[3]
    assert subscriptions.__len__() == 4
    assert rust_project_0.id == 29136820
    assert rust_project_0.labels == ["Deliverance Team"]
    assert rust_project_1.id == 29
    assert rust_project_1.labels == ["Deliverance Team", "Bar Team"]
    assert npm_project_0.id == 29136913
    assert npm_project_0.labels == ["Deliverance Team"]
    assert npm_project_1.id == 291
    assert npm_project_1.labels == ["Deliverance Team", "Foo Team"]


def test_deserialize_subscribed_projects_reads_expected_toml_structure_rust_only():
    # Given
    toml_str = """[[rust]]
id=1
labels=["Deliverance Team"]
"""
    toml_dict = toml.loads(toml_str)
    # When
    subscriptions = deserialize_subscribed_projects(toml_dict)
    # Then
    rust_project_0 = subscriptions[0]
    assert subscriptions.__len__() == 1
    assert rust_project_0.id == 1
    assert rust_project_0.labels == ["Deliverance Team"]


def test_deserialize_subscribed_projects_reads_expected_toml_structure_npm_only():
    # Given
    toml_str = """[[npm]]
id=2
labels=["Deliverance Team"]
"""
    toml_dict = toml.loads(toml_str)
    # When
    subscriptions = deserialize_subscribed_projects(toml_dict)
    # Then
    npm_project_0 = subscriptions[0]
    assert subscriptions.__len__() == 1
    assert npm_project_0.id == 2
    assert npm_project_0.labels == ["Deliverance Team"]


def test_deserialize_subscribed_projects_reads_expected_toml_structure_when_empty():
    # Given
    toml_str = ""
    toml_dict = toml.loads(toml_str)
    # When
    subscriptions = deserialize_subscribed_projects(toml_dict)
    # Then
    assert subscriptions.__len__() == 0


def test_deserialize_subscribed_projects_skips_project_with_missing_project_info():
    # Given
    toml_str = """[[rust]]
labels=["Deliverance Team"]

[[npm]]
id=29136913
"""
    toml_dict = toml.loads(toml_str)
    # When
    subscriptions = deserialize_subscribed_projects(toml_dict)
    # Then
    assert subscriptions.__len__() == 0
