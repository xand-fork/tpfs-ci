import os
from pathlib import Path

from ci_autoupdate.tool_logging import (
    AutoUpdateEvent,
    AutoUpdateEventResult,
    AutoUpdateLogger,
    AutoUpdateLoggerOutputFile,
)


def test_logger_output_file_set_and_created_correctly():
    # When
    logger = AutoUpdateLogger(AutoUpdateLoggerOutputFile(Path("test_directory"), "test.log"))
    # Then
    assert logger.output_file.abs == Path("test_directory/test.log").absolute()
    assert logger.output_file.abs.exists()
    # Cleanup test file and dir
    os.remove(logger.output_file.abs)
    os.removedirs(Path("test_directory"))


def test_logger_console_output_is_written_to(capsys):
    # Given
    logger = AutoUpdateLogger()
    # When
    logger.log_info("Arbitrary Message")
    out, err = capsys.readouterr()
    # Then
    assert "Arbitrary Message" in out
    assert AutoUpdateEvent.NONE.name in out
    assert AutoUpdateEventResult.NONE.name in out
    assert err == ""


def test_logger_output_file_is_written_to():
    # Given
    logger = AutoUpdateLogger(AutoUpdateLoggerOutputFile(Path("test_directory"), "test.log"))
    # When
    logger.log_info("Arbitrary Message")
    with open("test_directory/test.log", "r") as log_file:
        log = log_file.read()
    # Then
    assert "Arbitrary Message" in log
    assert AutoUpdateEvent.NONE.name in log
    assert AutoUpdateEventResult.NONE.name in log
    # Cleanup test file and dir
    os.remove(logger.output_file.abs)
    os.removedirs(Path("test_directory"))
