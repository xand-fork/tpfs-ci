import yaml

from ci_autoupdate.ci_version_replacement import replace_ref_in_yaml


def test_replace_ref_in_yaml_replaces_version_correctly():
    # Given
    yaml_s = """include:
  - project: 'transparentincdevelopment/product/devops/tpfs-ci'
    ref: v0.0.1
    file: '/templates/node/gitlab-ci.yaml'
"""
    loaded_yaml = yaml.safe_load(yaml_s)
    # When
    replaced_yaml = replace_ref_in_yaml(loaded_yaml, "v0.0.0")
    # Then (Key order is not important to YAML spec and pyyaml does not attempt to maintain order
    # so just check for existence of ref in a nested state of two spaces)
    replaced_yaml_s = yaml.dump(replaced_yaml, default_flow_style=False)
    assert "  ref: v0.0.0" in replaced_yaml_s
    assert "  ref: v0.0.1" not in replaced_yaml_s


def test_replace_ref_in_yaml_replaces_multiple_versions_correctly():
    # Given
    yaml_s = """include:
  - project: 'transparentincdevelopment/product/devops/tpfs-ci'
    ref: v0.0.1
    file: '/templates/node/gitlab-ci.yaml'
  - project: 'transparentincdevelopment/product/devops/tpfs-ci'
    ref: v0.0.1
    file: '/templates/rust/gitlab-ci.yaml'
"""
    loaded_yaml = yaml.safe_load(yaml_s)
    # When
    replaced_yaml = replace_ref_in_yaml(loaded_yaml, "v0.0.0")
    # Then (Key order is not important to YAML spec and pyyaml does not attempt to maintain order
    # so just check for existence of ref in a nested state of two spaces)
    replaced_yaml_s = yaml.dump(replaced_yaml, default_flow_style=False)
    assert replaced_yaml["include"][0]["ref"] == "v0.0.0"
    assert replaced_yaml["include"][1]["ref"] == "v0.0.0"
    assert "  ref: v0.0.1" not in replaced_yaml_s
