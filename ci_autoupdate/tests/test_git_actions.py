import os

from ci_autoupdate.git_actions import create_clone_url


def test_create_clone_url_creates_expected_string():
    # Given
    os.environ["TEST_USERNAME_ENV"] = "foo"
    os.environ["TEST_ACCESS_ENV"] = "bar"
    # When
    clone_url = create_clone_url("https://gitlab.com/transparent/foo/bar.git", "TEST_USERNAME_ENV", "TEST_ACCESS_ENV")
    # Then
    assert clone_url == "https://foo:bar@gitlab.com/transparent/foo/bar.git"
    # Cleanup temporary env vars
    os.environ["TEST_USERNAME_ENV"] = ""
    os.environ["TEST_ACCESS_ENV"] = ""
