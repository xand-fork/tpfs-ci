SHELL := /bin/bash

#
#	CI_AUTOUPDATE DOCKER IMAGE HANDLING
#

# Image / Container Data
NAME := ci-autoupdate
CONTAINER_NAME := $(NAME)
FULL_NAME := xandkeeper/$(NAME)
# https://stackoverflow.com/questions/6318809
VERSION := $(shell sed -nr "/^\[tool.poetry\]/ { :l /^version[ ]*=/ { s/.*=[\"\"]*//; p; q;}; n; b l;}" ./pyproject.toml | tr -d '"' | tr -d ' ')

# Possible Image:Tag combinations
STABLE_TAG := $(FULL_NAME):$(VERSION)
LATEST_TAG := $(FULL_NAME):latest
CUSTOM_TAG_PREPEND := $(FULL_NAME)

# TARGET
# Variable used in Docker build and publish commands to determine what name:tag combo/s to operate on
# Valid values:
# 	STABLE 			- Uses FULL_NAME:VERSION and FULL_NAME:latest
#	(Value) 		- Uses FULL_NAME:Value
TARGET_STABLE := STABLE

build:
	@if [[ -z "${TARGET}" ]]; then \
		echo 'Error: TARGET var not defined.  Define TARGET= as one of STABLE or a custom tag.'; \
		exit 1; \
	elif [[ "${TARGET}" = "${TARGET_STABLE}" ]]; then \
		echo "Building ${STABLE_TAG} and ${LATEST_TAG}"; \
		docker build -f Dockerfile -t ${STABLE_TAG} -t ${LATEST_TAG} . ; \
	else \
		echo "Building ${CUSTOM_TAG_PREPEND}:${TARGET}"; \
		docker build -f Dockerfile -t ${CUSTOM_TAG_PREPEND}:${TARGET} .; \
	fi


publish: build
	@if [[ -z "${TARGET}" ]]; then \
		echo 'Error: TARGET var not defined.  Define TARGET= as one of STABLE or a custom tag.'; \
		exit 1; \
	elif [[ "${TARGET}" = "${TARGET_STABLE}" ]]; then \
		echo "Publishing ${STABLE_TAG} and ${LATEST_TAG}"; \
		docker push ${STABLE_TAG}; \
		docker push ${LATEST_TAG}; \
	else \
		echo "Publishing ${CUSTOM_TAG_PREPEND}:${TARGET}"; \
		docker push ${CUSTOM_TAG_PREPEND}:${TARGET}; \
	fi
