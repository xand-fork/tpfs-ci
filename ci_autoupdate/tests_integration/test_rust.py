from gitlab.v4.objects.projects import Project  # type: ignore
from harness import cloned_rust_project_harness  # NOQA
from harness import INTEG_TEST_CLONE_DIRECTORY, INTEG_TEST_RUST_SUBSCRIPTION_FILE_PATH, TestData

from ci_autoupdate.cli_args import AutoUpdateConfig
from ci_autoupdate.run import run_tool


def test_rust_project_autoupdate(cloned_rust_project_harness):  # NOQA
    # Given a new branch pushed to the remote
    print("Test starting")
    test_data: TestData = cloned_rust_project_harness
    config = AutoUpdateConfig(
        "v0.0.1",
        False,
        test_data.test_branch_name_prefix,
        INTEG_TEST_RUST_SUBSCRIPTION_FILE_PATH,
        INTEG_TEST_CLONE_DIRECTORY,
    )

    # When
    run_tool(config)

    # Then
    project: Project = test_data.connection.projects.get(test_data.project.id)
    mrs = project.mergerequests.list(state="opened")
    found_project = False
    found_mr = False
    delete_source_branch_option_selected = False
    no_required_approvals = False
    for mr_result in mrs:
        found_project = found_project or mr_result.project_id == test_data.project.id
        found_mr = found_mr or mr_result.source_branch == test_data.test_branch_name_prefix + "-autoupdate"
        if found_mr:
            delete_source_branch_option_selected = mr_result.force_remove_source_branch
            no_required_approvals = mr_result.approvals.get().approvals_required == 0
    assert found_project and found_mr and delete_source_branch_option_selected and no_required_approvals
    # MR will automatically be "Closed" when branch is deleted during test cleanup
