ADO Feature tracking these TODOs: [Feature 6876](https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6876)

# Priority 1 TODOs:

- Improve CI autoupdate CI jobs by:
    - Creating a CI image which can install and run the poetry project from source OR
    - Sharing one built docker image between jobs
- use CLI parser (argparse) for taking args and specifying defaults (ADO 7125)
    - path to (possibly several) TPFS CI template import files instead of the default in the README
    - path to subscriptions.toml file
    - gitlab tokens / username
    - dry-run option
- Refactor CI templates, isolate to version separately (ADO 7123 and ADO 7124)
- Isolate CI Autoupdate from TPFS-CI (ADO 7123)

# Priority 2 TODOs:
- Look at removing " # type: ignore" from gitlab imports (No ADO)
- run `cargo lint-fix` per rust repo if subscription indicates and commit changes (ADO 7122)
- run `cargo clippy-fix` per rust repo if subscription indicates and commit changes (ADO 7122)
    - (this will mean running the program via dockerfile for all dependencies)


# Priority 3 TODOs:  
- Alert if MR pipeline fails
- run predetermined (or supplied) lint fix npm script per npm repo if subscription indicates and commit changes (may need to change lint-fix expected value in the subscriptions toml from a boolean to a string value representing the npm script name to run for the project) (ADO 7122)



# Notes
- To version templates separately (npm, rust, all purpose, python, etc) we need to split into separate projects (see - R/D setup)
