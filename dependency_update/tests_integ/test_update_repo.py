import unittest
import os

from datetime import datetime

from dependency_update.update_repo import update_repo
from dependency_update.update_strategies.cargo_update import CargoUpdate

from dependency_update.constants import GITLAB_URL, GITLAB_TOKEN_ENV_VAR
from dependency_update.gitlab_utils import try_create_gitlab_connection

import random
import string

INTEG_TEST_REPO_ID = 40329137


class Tests(unittest.TestCase):
    def test(self):
        test_branch = f'update-integ-{datetime.now().strftime("%Y-%m-%dT%H-%M-%S")}'
        os.environ["NEW_BRANCH_NAME"] = test_branch
        random_tag = ''.join(random.choices(string.ascii_lowercase, k=10))

        update_repo("ci-integ-testing", CargoUpdate(["xand-banks"], f"15.1.2-{random_tag}"))

        gl = try_create_gitlab_connection(GITLAB_URL, GITLAB_TOKEN_ENV_VAR)
        project = gl.projects.get(INTEG_TEST_REPO_ID)
        mrs = project.mergerequests.list(source_branch=test_branch)
        mr = mrs[0]
        try:
            assert mr
            assert mr.merge_when_pipeline_succeeds
            assert mr.title == "Update entries for 'xand-banks'"
            assert mr.labels == ["Team Cookie Monster"]
            assert mr.description == "Updating entries for 'xand-banks'"
            assert mr.should_remove_source_branch
            assert mr.approvals_before_merge is None
        finally:
            project.branches.delete(test_branch)
