import os
import unittest

from unittest.mock import patch
from datetime import datetime
from dependency_update.update_dependency_registry import update_registry_repo
from dependency_update.gitlab_utils import try_create_gitlab_connection
from dependency_update.constants import GITLAB_URL, GITLAB_TOKEN_ENV_VAR

INTEG_TEST_REPO_ID = 40329137


class Tests(unittest.TestCase):
    @patch('dependency_update.git_utils.registry_repo_url.__defaults__',
           ('gitlab.com/TransparentIncDevelopment/r-d/ci-integ-testing.git',))
    @patch('dependency_update.git_utils.merge.__defaults__', (INTEG_TEST_REPO_ID,))
    @patch('dependency_update.git_utils.remove_branch.__defaults__', (INTEG_TEST_REPO_ID,))
    def test_write_to_repo(self):
        test_branch = f'registry-integ-{datetime.now().strftime("%Y-%m-%dT%H-%M-%S")}'
        os.environ["NEW_BRANCH_NAME"] = test_branch
        consumer = "registry-update-test"
        deps = ["test-dependency"]
        update_registry_repo(consumer, deps, True)
        gl = try_create_gitlab_connection(GITLAB_URL, GITLAB_TOKEN_ENV_VAR)
        project = gl.projects.get(INTEG_TEST_REPO_ID)
        mr = project.mergerequests.list(source_branch=test_branch)[0]

        try:
            assert mr
            assert mr.merge_when_pipeline_succeeds
            assert mr.title == "Update entries for 'registry-update-test'"
            assert mr.labels == ["Team Cookie Monster"]
            assert mr.description == "Updating entries for 'registry-update-test'"
            assert mr.should_remove_source_branch
            assert mr.approvals_before_merge is None
        finally:
            project.branches.delete(test_branch)
