"""
Tasks to be invoked via "poetry run".
Any task added here must also be specified as a "script" in pyproject.toml.
"""

import os
import subprocess
import sys
import argparse
from typing import List, Union


REPO_ROOT = os.path.dirname(__file__)


def _command(command: Union[List[str], str], shell: bool = False):
    command_exit_code = subprocess.call(
        command,
        cwd=REPO_ROOT,
        shell=shell,
    )

    if command_exit_code != 0:
        sys.exit(command_exit_code)


def tests():
    _command(["python", "-m", "unittest",  "discover", "-b", "tests"])


def tests_integ():
    _command(["python", "-m", "unittest", "discover", "-b", "tests_integ"])
