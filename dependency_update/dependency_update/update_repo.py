import os
import sys

from dependency_update.constants import GITLAB_USER_NAME_ENV_VAR, GITLAB_TOKEN_ENV_VAR, MASTER_BRANCH, \
    GITLAB_URL
from dependency_update.gitlab_utils import try_create_gitlab_connection
from dependency_update.git_utils import pull_down_registry_repo, pull_repo, \
    checkout_new_branch, push_branch, remove_branch, merge, clean_up_repo, registry_repo_url

from dependency_update.update_strategies.update_strategies import UpdateStrategy

TPFS_GROUP_ID = 3465402

def _extract_repo_url(http_url: str) -> str:
    return http_url.split('//')[1]


def _find_matching_repo(repo_name: str, projects: []):
    if len(projects) == 0:
        print(f"No projects found.")
    else:
        if len(projects) > 1:
            print("Multiple matching projects found.")
            for project in projects:
                if project.name == repo_name:
                    print(f"Picking nearest matching project: '{project.name}'")
                    return project
        elif projects[0].name == repo_name:
            return projects[0]
        else:
            print(f"No project matching '{repo_name}' found.")


def _get_gitlab_project(repo_name: str):
    gl = try_create_gitlab_connection(GITLAB_URL, GITLAB_TOKEN_ENV_VAR)
    group = gl.groups.get(TPFS_GROUP_ID)
    projects = group.projects.list(search=repo_name, archived=False, include_subgroups=True)
    return _find_matching_repo(repo_name, projects)


def update_repo(repo_name: str, update_strategy: UpdateStrategy):
    project = _get_gitlab_project(repo_name)

    if project:
        repo_url = registry_repo_url(_extract_repo_url(project.http_url_to_repo))
        repo, repo_path = pull_repo(repo_url, repo_name, MASTER_BRANCH)
        try:
            new_branch = checkout_new_branch(repo)
            if update_strategy.update(repo_path):
                push_branch(repo, update_strategy.package_names, new_branch, repo_path)
                try:
                    merge(new_branch, update_strategy.package_names, project.id)
                except Exception as e:
                    print("Failed to merge")
                    print(e)
                    remove_branch(new_branch, project.id)
        except Exception as e:
            print("Failed to update repo")
            print(e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
        finally:
            clean_up_repo(repo_path)
    else:
        print("No projects found")


