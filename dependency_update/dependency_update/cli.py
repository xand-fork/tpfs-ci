import argparse
import os
import sys
from functools import reduce
from itertools import chain

from dependency_update.cargo_dependency_extractor import extract_dependencies_recursive
from dependency_update.read_dependency_registry import read_crate_consumers_from_remote_repo
from dependency_update.update_dependency_registry import update_registry_repo
from dependency_update.update_repo import update_repo
from dependency_update.update_strategies.cargo_update import CargoUpdate


def sync_cargo_dependencies(args: argparse.Namespace) -> None:
    print(f"Dependency_update CLI: sync_cargo_dependencies")

    repository_name = args.repository_name
    print(f"Synchronizing TPFS dependencies for repo '{repository_name}'.")

    consolidated_dependencies = extract_dependencies_recursive(args.cargo_toml_search_path)
    print(f"Found {len(consolidated_dependencies)} dependencies.")

    update_registry_repo(repository_name, consolidated_dependencies)
    print("Finished synchronizing dependencies.")

def downstream_merges(args: argparse.Namespace) -> None:
    print(f"Starting downstream merges for:")

    upstream_crates = args.upstream_crates
    upstream_repo_version = args.upstream_repo_version
    print(f"\tupstream_crates: '{upstream_crates}'")
    print(f"\tupstream_repo_version: '{upstream_repo_version}'")

    consuming_repos_by_crate = map(read_crate_consumers_from_remote_repo, upstream_crates)
    consuming_repos = list(sorted(set(reduce(chain, consuming_repos_by_crate, []))))
    if not consuming_repos:
        print("No downstream consumers found.")
        return

    print(f"Found downstream consumers: '{consuming_repos}'")
    for consuming_repo in consuming_repos:
        if consuming_repo not in args.excluded_repos:
            print(f"Starting downstream merge into: '{consuming_repo}'")
            update_repo(consuming_repo, CargoUpdate(upstream_crates, upstream_repo_version))
    print(f"Finished merging consumers of '{upstream_crates}'.")


def main():
    parser = parser = argparse.ArgumentParser(prog='depenedency_update')  # here we turn off default help action
    subparsers = parser.add_subparsers(title='subcommands', description='valid subcommands', help='additional help')

    sync_parser = subparsers.add_parser('sync-dependencies', help='Synchronizes Cargo dependencies into central registry')
    sync_parser.set_defaults(func=sync_cargo_dependencies)
    sync_parser.add_argument('repository_name', type=str, help='Name of consuming repository')
    sync_parser.add_argument('cargo_toml_search_path',
                        type=str,
                        default=os.getcwd(),
                        nargs='?',
                        help='Root path to recursively search for Cargo.tomls')

    downstream_merge_parser = subparsers.add_parser('upgrade-consumers', help='Identifies and upgrades downstream consumers with latest version of given crate')
    downstream_merge_parser.set_defaults(func=downstream_merges)
    downstream_merge_parser.add_argument('--exclude', '-e', type=str, action='append', dest='excluded_repos', default=[])
    downstream_merge_parser.add_argument('upstream_repo_version', type=str, help='Version of repo. In case on multiple crates they must share the same version.')
    downstream_merge_parser.add_argument('upstream_crates', nargs='+', help='Names of upstream crates. One or more required.')

    # Print help when no CLI args are passed
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
