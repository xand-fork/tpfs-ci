import os
from typing import Union
from gitlab import Gitlab, GitlabAuthenticationError


def try_create_gitlab_connection(
        server_url: str,
        access_token_env_key: str,
) -> Union[Gitlab, None]:
    try:
        private_token = os.getenv(access_token_env_key)
        gitlab_connection = Gitlab(server_url, private_token=private_token)
        gitlab_connection.auth()
        return gitlab_connection
    except GitlabAuthenticationError as ex:
        raise ex

