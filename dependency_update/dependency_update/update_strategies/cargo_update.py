from subprocess import run

import tomli
import tomli_w
import os

from dependency_update.update_strategies.tomledit import surgical_edit

READ_ONLY = 'rb'
WRITE = 'wb'


def _is_workspace(cargo_dir: str) -> bool:
    cargo_file = os.path.join(cargo_dir, "Cargo.toml")

    with open(cargo_file, READ_ONLY) as f:
        toml_dict = tomli.load(f)
        return 'workspace' in toml_dict


def _get_workspace_members(workspace_dir: str) -> []:
    workspace_cargo_file = os.path.join(workspace_dir, "Cargo.toml")

    with open(workspace_cargo_file, READ_ONLY) as f:
        toml_dict = tomli.load(f)
        return toml_dict['workspace']['members']


def _overwrite_dependencies(cargo_file: str, packages: [str], version: str) -> bool:
    update_tomlfile = False

    print(f"Searching cargo toml for matching dependencies: {cargo_file}")
    with open(cargo_file, READ_ONLY) as f:
        toml_dict = tomli.load(f)
        for header in ['dependencies', 'dev-dependencies', 'build-dependencies']:
            for package in packages:
                if header in toml_dict \
                        and package in toml_dict[header] \
                        and toml_dict[header][package]['version'] != version:
                    print(f"Found dependency")
                    update_tomlfile = True
                    surgical_edit(cargo_file, f"{header}.{package}.version", version)
    return update_tomlfile


def _lockfile_exists(manifest_path: str) -> bool:
    lockfile = os.path.join(manifest_path, "Cargo.lock")
    return os.path.exists(lockfile)


class CargoUpdate:
    def __init__(self, package_names: [str], version: str):
        self.package_names = package_names
        self.version = version

    def update_deps(self, manifest_path: str):
        cargofile = os.path.join(manifest_path, "Cargo.toml")

        deps_updated = False
        if _is_workspace(manifest_path):
            print("Found workspace")
            for member in _get_workspace_members(manifest_path):
                member_cargo_file = os.path.join(manifest_path, member, "Cargo.toml")
                deps_updated = _overwrite_dependencies(member_cargo_file, self.package_names, self.version) or deps_updated
        else:
            deps_updated = _overwrite_dependencies(cargofile, self.package_names, self.version)
        return deps_updated

    def update(self, manifest_path: str):
        if self.update_deps(manifest_path):
            # Since cargo update only updates the Cargo.lock file, we'll only run it if we've got the lockfile in git
            if _lockfile_exists(manifest_path):
                for package_name in self.package_names:
                    run(['cargo', 'update', '-p', package_name, '--precise', self.version], cwd=manifest_path)
            else:
                print("Lockfile git-ignored. Skipping cargo update.")
            return True
        else:
            print("No dependencies on " + self.package_names)
            return False
