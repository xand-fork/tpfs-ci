from enum import Enum

from dependency_update.update_strategies.cargo_update import CargoUpdate


class UpdateStrategy(Enum):
    CARGO_UPDATE = CargoUpdate
