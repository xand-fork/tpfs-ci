import os
import subprocess
import sys

TOMLEDIT_RUST_SCRIPT = os.path.dirname(__file__) + "/tomledit.rs"

def surgical_edit(cargo_file_path: str, edit_location: str, value: str):
    command_exit_code = subprocess.call(
        ["rust-script", TOMLEDIT_RUST_SCRIPT, cargo_file_path, edit_location, value],
        shell=False
    )
    if command_exit_code != 0:
        raise Exception(f"tomledit failed! exit code: {command_exit_code}")


