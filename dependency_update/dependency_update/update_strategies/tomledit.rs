#!/usr/bin/env rust-script
//! This is a regular crate doc comment, but it also contains a partial
//! Cargo manifest.  Note the use of a *fenced* code block, and the
//! `cargo` "language".
//!
//! ```cargo
//! [dependencies]
//! tomllib = "0.1.2"
//! structopt = { version = "0.3", default-features = false }
//! ```
//!
use tomllib::TOMLParser;
use tomllib::types::Value;
use std::fs;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "example", about = "An example of StructOpt usage.")]
struct Opt {
    /// Cargo.toml file to edit
    #[structopt(parse(from_os_str))]
    cargo_file: PathBuf,

    /// Where to write the output: to `stdout` or `file`
    #[structopt()]
    edit_location: String,

    /// Where to write the output: to `stdout` or `file`
    #[structopt()]
    value: String,
}

fn main() {
    let opt = Opt::from_args();

    let contents = fs::read_to_string(&opt.cargo_file)
        .expect("Should have been able to read the file");

    let parser = TOMLParser::new();
    let (mut parser, _result) = parser.parse(&contents);
    parser.set_value(opt.edit_location, Value::basic_string(opt.value).expect("Unable to set value at edit location"));

    fs::write(opt.cargo_file, parser.to_string()).expect("Unable to write cargo file");
    std::process::exit(0);
}



