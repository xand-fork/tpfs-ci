from typing import Dict, List
import unittest
import os
import tomli
import tomli_w
import copy

from dependency_update.constants import RELATIVE_PATH_TO_REGISTRY

CRATE_DEPENDENCIES_HEADER = "crate-dependencies"

RegistryToml = Dict[str, Dict[str, List[str]]]


class Registry:
    def __init__(self, toml: RegistryToml):
        self.toml = toml[CRATE_DEPENDENCIES_HEADER]

    # Updates all specified deps to include consumer and removes consumer from any others
    def update_deps(self, consumer: str, update_deps: List[str]) -> bool:
        old_toml = copy.deepcopy(self.toml)
        for dep in self.toml:
            if dep in update_deps:
                if consumer not in self.toml[dep]:
                    self.toml[dep].append(consumer)
                update_deps.remove(dep)
            else:
                if consumer in self.toml[dep]:
                    self.toml[dep].remove(consumer)
        for remaining_dep in update_deps:
            self.toml.update({remaining_dep: [consumer]})

        return self.is_different(old_toml)

    def as_toml(self) -> RegistryToml:
        return {CRATE_DEPENDENCIES_HEADER: self.toml}

    def get_consumers_of_dep(self, dep) -> List[str]:
        if dep in self.toml:
            return self.toml[dep]
        else:
            return []

    def is_different(self, old_toml: Dict[str, List[str]]) -> bool:
        return self.toml != old_toml


def read_registry(registry_path: str) -> Registry:
    with open(registry_path, "rb") as f:
        toml_dict = tomli.load(f)
    return Registry(toml_dict)


def write_registry(registry_path: str, registry: Registry):
    registry_toml = registry.as_toml()
    with open(registry_path, "rb") as f:
        toml_dict = tomli.load(f)
    toml_dict[CRATE_DEPENDENCIES_HEADER] = registry_toml[CRATE_DEPENDENCIES_HEADER]
    with open(registry_path, "wb") as f:
        tomli_w.dump(toml_dict, f)
