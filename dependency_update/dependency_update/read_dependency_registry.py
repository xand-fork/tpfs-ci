import os
from git import Repo
from typing import List

from dependency_update.constants import CRATE_DEPENDENCY_REGISTRY_REPO_URL, CRATE_DEPENDENCY_REGISTRY_REPO_NAME, \
    RELATIVE_PATH_TO_REGISTRY, MASTER_BRANCH, CRATE_DEPENDENCIES_HEADER
from dependency_update.registry import read_registry

from dependency_update.git_utils import clean_up_repo


class RegistryRepoConfig:
    def __init__(self, repo_url, repo_name, relative_path_to_registry, branch):
        self.repo_url = repo_url
        self.repo_name = repo_name
        self.relative_path_to_registry = relative_path_to_registry
        self.branch = branch


def init_crate_dependencies_registry_repo_config():
    return RegistryRepoConfig(CRATE_DEPENDENCY_REGISTRY_REPO_URL,
                              CRATE_DEPENDENCY_REGISTRY_REPO_NAME,
                              RELATIVE_PATH_TO_REGISTRY,
                              MASTER_BRANCH)

#

def read_crate_consumers_from_remote_repo(crate_name,
                                          registry_repo_config_factory=init_crate_dependencies_registry_repo_config) -> List[str]:
    registry_repo_config = registry_repo_config_factory()
    path_to_registry_repo = pull_crate_dependency_registry_repo(registry_repo_config.repo_url,
                                                                registry_repo_config.repo_name,
                                                                registry_repo_config.branch)
    path_to_registry = os.path.join(path_to_registry_repo, registry_repo_config.relative_path_to_registry)
    consumers = read_crate_consumers_from_toml(crate_name, path_to_registry)

    clean_up_repo(path_to_registry_repo)

    return consumers



def pull_crate_dependency_registry_repo(repo_url, repo_name, branch):
    cwd = os.getcwd()
    path_to_registry_repo = os.path.join(cwd, repo_name)
    Repo.clone_from(repo_url, path_to_registry_repo, branch=branch)
    return path_to_registry_repo


def read_crate_consumers_from_toml(crate_name, file_path) -> List[str]:
    registry = read_registry(file_path)
    return registry.get_consumers_of_dep(crate_name)
