from dependency_update.cargo_dependency_extractor import extract_dependencies, extract_dependencies_by_filepath, extract_dependencies_recursive
import tempfile
import unittest
import os
from typing import List

from utils import write_toml_file


class TestCargoDependencyExtractor(unittest.TestCase):

    def extract_deps_by_path(self, toml_content: str) -> List[str]:
        with tempfile.NamedTemporaryFile(mode = "w") as temp:
            temp.write(toml_content)
            temp.flush()
            return extract_dependencies_by_filepath(temp.name)

    def test_extract_with_empty_dependencies_returns_empty_list(self):
        toml_content = """
[package]
name = "name"
        
[dependencies]
"""
        dependencies = self.extract_deps_by_path(toml_content)
        assert len(dependencies) == 0

    def test_extract_workspace_toml_returns_sub_crate(self):
        with tempfile.TemporaryDirectory() as temp_base_folder:

            workspace_toml_content = """
[workspace]

members = [
    "crate1"
]
"""
            workspace_cargo_file = os.path.join(temp_base_folder, "Cargo.toml")
            write_toml_file(workspace_cargo_file, workspace_toml_content)

            crate1_toml_str = """
[package]
name = "crate1"

[dependencies]
some-tpfs-crate = { version = "1.2.3", registry = "tpfs" }
"""
            crate1_cargo_file = os.path.join(temp_base_folder, "crate1", "Cargo.toml")
            write_toml_file(crate1_cargo_file, crate1_toml_str)

            dependencies = extract_dependencies_recursive(temp_base_folder)

            assert dependencies == ['some-tpfs-crate']

    def test_extract_skips_crates_io_dependencies(self):
        toml_str = """
[package]
name = "name"

[dependencies]
thing-one = "1.2.3"
thing-two = "4.5.6"
thing-three = "1.1.1"
"""
        dependencies = self.extract_deps_by_path(toml_str)

        assert len(dependencies) == 0

    def test_extract_skips_dependencies_without_registry(self):
        toml_str = """
[package]
name = "name"

[dependencies]
non-tpfs-crate = { version = "1.2.3", features = ["full"] }
"""
        dependencies = self.extract_deps_by_path(toml_str)

        assert len(dependencies) == 0

    def test_extract_returns_tpfs_crates_by_tpfs_registry(self):
        toml_str = """
[package]
name = "name"

[dependencies]
tpfs-crate = { version = "1.2.3", registry = "tpfs" }
"""
        dependencies = self.extract_deps_by_path(toml_str)

        assert dependencies == ['tpfs-crate']

    def test_extract_skips_dependencies_with_non_tpfs_registry(self):
        toml_str = """
[package]
name = "name"

[dependencies]
non-tpfs-crate = { version = "1.2.3", registry = "not-tpfs" }
"""
        dependencies = self.extract_deps_by_path(toml_str)

        assert len(dependencies) == 0

    def test_recursively_finds_single_cargo_in_base_folder(self):
        base_toml_str = """
[package]
name = "name"
    
[dependencies]
some-tpfs-crate = { version = "1.2.3", registry = "tpfs" }
"""
        with tempfile.TemporaryDirectory() as temp_base_folder:
            base_cargo_file = os.path.join(temp_base_folder, "Cargo.toml")
            write_toml_file(base_cargo_file, base_toml_str)

            dependencies = extract_dependencies_recursive(temp_base_folder)

            assert len(dependencies) == 1

    def test_extract_sorts_dependencies_alphabetically_by_name(self):
        base_toml_str = """
    [package]
    name = "name"
    
    [dependencies]
    bear = { version = "4.5.3", registry = "tpfs" }
    zebra = { version = "0.9.9", registry = "tpfs" }
    antelope = { version = "1.2.3", registry = "tpfs" }
    honeybadger = { version = "10.5.30", registry = "tpfs" }
    """
        with tempfile.TemporaryDirectory() as temp_base_folder:
            base_cargo_file = os.path.join(temp_base_folder, "Cargo.toml")
            write_toml_file(base_cargo_file, base_toml_str)

            dependencies = extract_dependencies_recursive(temp_base_folder)

            assert dependencies == ['antelope', 'bear', 'honeybadger', 'zebra']

    def test_recursively_finds_many_cargo_in_base_folder(self):
        with tempfile.TemporaryDirectory() as temp_base_folder:

            base_toml_str = """
        [package]
        name = "name"
            
        [dependencies]
        some-tpfs-crate = { version = "1.2.3", registry = "tpfs" }
        """
            base_cargo_file = os.path.join(temp_base_folder, "Cargo.toml")
            write_toml_file(base_cargo_file, base_toml_str)

            nested_toml_str = """
        [package]
        name = "another-name"
            
        [dependencies]
        another-tpfs-crate = { version = "1.2.3", registry = "tpfs" }
        """
            nested_cargo_file = os.path.join(temp_base_folder, "subfolder-a", "subfolder-b", "Cargo.toml")
            write_toml_file(nested_cargo_file, nested_toml_str)

            dependencies = extract_dependencies_recursive(temp_base_folder)

            assert dependencies == ['another-tpfs-crate', 'some-tpfs-crate']

    def test_recursive_search_de_duplicates_dependencies_among_all_crates(self):
        with tempfile.TemporaryDirectory() as temp_base_folder:

            base_toml_str = """
            [package]
            name = "name"
            
            [dependencies]
            some-tpfs-crate = { version = "1.2.3", registry = "tpfs" }
            """
            base_cargo_file = os.path.join(temp_base_folder, "Cargo.toml")
            write_toml_file(base_cargo_file, base_toml_str)

            nested_toml_str = """
            [package]
            name = "another-name"
    
            [dependencies]
            some-tpfs-crate = { version = "1.2.3", registry = "tpfs" }
            """
            nested_cargo_file = os.path.join(temp_base_folder, "subfolder-a", "subfolder-b", "Cargo.toml")
            write_toml_file(nested_cargo_file, nested_toml_str)

            dependencies = extract_dependencies_recursive(temp_base_folder)

            assert dependencies == ['some-tpfs-crate']

    def test_recursive_search_on_empty_folder(self):
        temp_base_folder = tempfile.TemporaryDirectory()

        dependencies = extract_dependencies_recursive(temp_base_folder.name)

        assert dependencies == []

    def test_extract_excludes_local_crates(self):
        with tempfile.TemporaryDirectory() as temp_base_folder:
            base_toml_str = """
            [package]
            name = "antelope"
        
            [dependencies]
            bear = { version = "4.5.3", registry = "tpfs" }
            """
            base_cargo_file = os.path.join(temp_base_folder, "Cargo.toml")
            write_toml_file(base_cargo_file, base_toml_str)

            nested_toml_str = """
            [package]
            name = "honeybadger"
    
            [dependencies]
            antelope = { version = "1.2.3", registry = "tpfs" }
            """
            nested_cargo_file = os.path.join(temp_base_folder, "subfolder-a", "subfolder-b", "Cargo.toml")
            write_toml_file(nested_cargo_file, nested_toml_str)

            dependencies = extract_dependencies_recursive(temp_base_folder)

            assert dependencies == ['bear']


if __name__ == '__main__':
    unittest.main()


