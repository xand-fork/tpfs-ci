import unittest

from dependency_update.update_repo import _find_matching_repo, _extract_repo_url


class TestProject:
    def __init__(self, name):
        self.name = name


class TestUpdateRepoStrategy(unittest.TestCase):

    def test__matches_correct_repo(self):
        awesomeco = TestProject("AwesomeCo")
        projects = [awesomeco, TestProject("AwesomeCo-ConfigFile")]
        assert (awesomeco == _find_matching_repo("AwesomeCo", projects))

    def test__extracts_repo_url(self):
        assert (_extract_repo_url("https://gitlab.com/TransparentIncDevelopment/r-d/test-crate.git") ==
                "gitlab.com/TransparentIncDevelopment/r-d/test-crate.git")
