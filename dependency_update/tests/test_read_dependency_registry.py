import os
import unittest
from dependency_update.read_dependency_registry import read_crate_consumers_from_toml

TEST_TOML_FILE_PATH = "tests/sample_registry.toml"


def create_test_toml(text):
    f = open(TEST_TOML_FILE_PATH, "w")
    f.write(text)
    f.close()


def delete_test_toml():
    os.remove(TEST_TOML_FILE_PATH)


class Tests(unittest.TestCase):
    def test_read_crate_dependencies_from_toml__existing_crate(self):
        # Given a valid crate name and file path
        test_toml_text = """
[crate-dependencies]
crate-1 = ["repo_1", "repo_2"]
"""
        create_test_toml(test_toml_text)
        crate = "crate-1"
        file_path = TEST_TOML_FILE_PATH

        # When
        res = read_crate_consumers_from_toml(crate, file_path)

        # Then
        assert ('repo_1' in res)
        assert ('repo_2' in res)
        delete_test_toml()

    def test_read_crate_dependencies_from_toml__no_such_crate(self):
        # Given a valid file path, but a crate name that doesn't exist in the toml
        test_toml_text = """
[crate-dependencies]
crate-1 = ["repo_1", "repo_2"]
"""
        create_test_toml(test_toml_text)
        crate = "dummy-crate"
        file_path = TEST_TOML_FILE_PATH

        # When
        res = read_crate_consumers_from_toml(crate, file_path)

        # Then
        assert res == []
        delete_test_toml()

    def test_read_crate_dependencies_from_toml__cannot_read_outside_of_header(self):
        # Given a valid file path, but a crate name that doesn't exist within the "crate-dependencies" header
        test_toml_text = """
crate-2 = ["repo_3"]
        
[crate-dependencies]
crate-1 = ["repo_1", "repo_2"]
"""
        create_test_toml(test_toml_text)
        crate = "crate-2"
        file_path = TEST_TOML_FILE_PATH

        # When
        res = read_crate_consumers_from_toml(crate, file_path)

        # Then
        assert res == []
        delete_test_toml()
